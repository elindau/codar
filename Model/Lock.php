<?php

namespace model;

class Lock {

	// 1min
	const MICRO_SEC = 60;
	const ENERGY_LOCK = 30;
	
	private $m_db = null;
	private $m_trainingDal = null;

	public function __construct(\DBAccess\Database $db) {
		$this->m_codarDal = new \DBAccess\CodarDAL($db);
		$this->m_trainingDal = new \DBAccess\TrainingDAL($db);
		$this->m_db = $db;
	}

	// Lås codar, lägger egenligen bara till ett id i databasen
	public function lockCodar(\model\Codar $codar) {

		// Uppdatera databasen
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_TRAINING_ID, \DBAccess\CodarDAL::COL_CODAR_TIMELOCKED);
		$equals = array($codar->getCodarTrainingId(), $codar->getTimeWhenLocked());
		$types = array(\DBAccess\CodarDAL::COL_CODAR_TRAINING_ID_TYPE, \DBAccess\CodarDAL::COL_CODAR_TIMELOCKED_TYPE);
		$outcome = $this->m_codarDal->updateCodar($codar, $arguments, $equals, $types);

		// Träning kostar 1 energi
		$this->energyHandler($codar, $codar->getEnergy() - 1);

		if(!($outcome)) {
			trigger_error(\Common\Strings::e_database);
		} 
	}

	/**
	* Hanterar energi på en codar
	* @param \model\Codar $codar 
	* @param int $energy 
	* @return boolean
	*/
	public function energyHandler(\model\Codar $codar, $energy) {
		
		$timeWhenLocked = 0;

		// Vid köp av energi och vid ev. energi-lås, ta bort timelocken om där inte finns träning
		if($codar->getEnergy() == \model\Codar::MIN_ENERGY && $energy > $codar->getEnergy() && $codar->getCodarTrainingId() == 0) {
			$arguments = array(\DBAccess\CodarDAL::COL_CODAR_TIMELOCKED);
			$equals = array($timeWhenLocked);
			$types = array(\DBAccess\CodarDAL::COL_CODAR_TIMELOCKED_TYPE);
			$outcome = $this->m_codarDal->updateCodar($codar, $arguments, $equals, $types);			
		}

		// Sätt energi till max-min värden
		if($energy < \model\Codar::MIN_ENERGY) {
			$energy = \model\Codar::MIN_ENERGY;
		}

		if($energy > \model\Codar::MAX_ENERGY) {
			$energy = \model\Codar::MAX_ENERGY;
		}

		// Är energin 0
		if($energy == \model\Codar::MIN_ENERGY) {
			// Lägg till timelock
			$timeWhenLocked = microtime(true);
			$arguments = array(\DBAccess\CodarDAL::COL_CODAR_TIMELOCKED);
			$equals = array($timeWhenLocked);
			$types = array(\DBAccess\CodarDAL::COL_CODAR_TIMELOCKED_TYPE);
			$outcome = $this->m_codarDal->updateCodar($codar, $arguments, $equals, $types);
		}

		// Uppdatera Codar'ns energi
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_ENERGY);
		$equals = array($energy);
		$types = array(\DBAccess\CodarDAL::COL_CODAR_ENERGY_TYPE);
		$outcome = $this->m_codarDal->updateCodar($codar, $arguments, $equals, $types);

		if(!($outcome)) {
			trigger_error(\Common\Strings::e_database);
		} 

	}

	// Hämtar ut hur länge codarn varit låst
	public function getCodarTimeLock($codarId) {

		// Hämta ut codarn
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_ID);
		$equals = array($codarId);
		$types = array(\DBAccess\CodarDAL::COL_CODAR_ID_TYPE);
		$codar = $this->m_codarDal->selectCodarBy($arguments, $equals, $types);

		if($codar->getCodarTrainingId() != 0) {
			// Hämta ut träningen
			$arguments = array(\DBAccess\TrainingDAL::COL_TRAINING_ID);
			$equals = array($codar->getCodarTrainingId());
			$types = array(\DBAccess\TrainingDAL::COL_TRAINING_ID_TYPE);
			$training = $this->m_trainingDal->selectTrainingBy($arguments, $equals, $types);

			$timePassed = microtime(true) - $codar->getTimeWhenLocked();
			$timeLock = $training->getTimelock() * self::MICRO_SEC;

			return round((($timeLock - $timePassed) / 60), 0);	

		} else if($codar->getEnergy() == 0) {
			$timePassed = microtime(true) - $codar->getTimeWhenLocked();
			$timeLock = self::ENERGY_LOCK * self::MICRO_SEC;

			return round((($timeLock - $timePassed) / 60), 0);	
		}

	}

	// Kollar om codarn är låst
	public function isLocked($codarId) {

		// Hämta ut codarn
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_ID);
		$equals = array($codarId);
		$types = array(\DBAccess\CodarDAL::COL_CODAR_ID_TYPE);
		$codar = $this->m_codarDal->selectCodarBy($arguments, $equals, $types);

		// Är han på träning
		if($codar->getCodarTrainingId() != 0) {
			// Hämta ut träningen
			$arguments = array(\DBAccess\TrainingDAL::COL_TRAINING_ID);
			$equals = array($codar->getCodarTrainingId());
			$types = array(\DBAccess\TrainingDAL::COL_TRAINING_ID_TYPE);
			$training = $this->m_trainingDal->selectTrainingBy($arguments, $equals, $types);

			$timePassed = microtime(true) - $codar->getTimeWhenLocked();
			$timeLock = $training->getTimelock() * self::MICRO_SEC;

			if($timeLock > $timePassed) {
				// Fortfarande på träning
				return true;
			} else {
				// Träning slut

				// Ta bort träning från databasen och lägg till exp
				$earnedExp = $codar->getCodarExp() + $training->getValue();
				// Lägg till exp genom den här funktionen istället
				$level = new \model\Level($codar, $this->m_db);
				$level->addExpToCodar($earnedExp);

				$arguments = array(\DBAccess\CodarDAL::COL_CODAR_TRAINING_ID, \DBAccess\CodarDAL::COL_CODAR_TIMELOCKED);
				$equals = array(0, 0);
				$types = array(\DBAccess\CodarDAL::COL_CODAR_TRAINING_ID_TYPE, \DBAccess\CodarDAL::COL_CODAR_TIMELOCKED_TYPE);
				$outcome = $this->m_codarDal->updateCodar($codar, $arguments, $equals, $types);

				if(!($outcome)) {
					trigger_error(\Common\Strings::e_database);
				}

				// Skicka meddelande till Codar att träningen är slut
				\model\MessageHandler::addMessage(new \model\Message(0, $codar->getCodarId(), "Träningen är slut!"), $this->m_db);

				// Codarn kan dock nu ha 0 energi och ska fortfarande vara låst
				if($codar->getEnergy() == 0) {
					// Lägg till timelock
					$timeWhenLocked = microtime(true);
					$arguments = array(\DBAccess\CodarDAL::COL_CODAR_TIMELOCKED);
					$equals = array($timeWhenLocked);
					$types = array(\DBAccess\CodarDAL::COL_CODAR_TIMELOCKED_TYPE);
					$outcome = $this->m_codarDal->updateCodar($codar, $arguments, $equals, $types);
					return true;
				}

				return false; 
			}

		} else if($codar->getEnergy() == 0) {

			$timePassed = microtime(true) - $codar->getTimeWhenLocked();
			$timeLock = self::ENERGY_LOCK * self::MICRO_SEC;
				if($timeLock > $timePassed) {
					// Fortfarande låst
					return true;
				} else {
					// Energi-lås slut
					$arguments = array(\DBAccess\CodarDAL::COL_CODAR_ENERGY, \DBAccess\CodarDAL::COL_CODAR_TIMELOCKED);
					$equals = array(\model\Codar::MAX_ENERGY, 0);
					$types = array(\DBAccess\CodarDAL::COL_CODAR_ENERGY_TYPE, \DBAccess\CodarDAL::COL_CODAR_TIMELOCKED_TYPE);
					$outcome = $this->m_codarDal->updateCodar($codar, $arguments, $equals, $types);

					if(!($outcome)) {
						trigger_error(\Common\Strings::e_database);
					}

					// Skicka meddelande till Codar att energin är återställd
					\model\MessageHandler::addMessage(new \model\Message(0, $codar->getCodarId(), "Energi återställd!"), $this->m_db);
					return false; 
				}
		} else {
			// Inte på träning
			return false;
		}
	}
}