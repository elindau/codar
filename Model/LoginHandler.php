<?php
namespace model;

class LoginHandler
{
	const STATE = "sessionstate";
	private $m_token = null;
	private $m_userDal = null;

	// Kollar så det finns en session, initierar databasen
	public function __construct(\DBAccess\Database $db) {
		
		$this->m_userDal = new \DBAccess\UserDAL($db);

		if(isset($_SESSION) == false) {
			throw new \Exception(\Common\Strings::e_sessionNotStarted);
		}
	}

	// Kollar om Sessionsvariablen är true
	public static function isLoggedIn(){
		if(isset($_SESSION[self::STATE])) {
			return true;
		}
		return false;
	}

	public static function getLoggedInUser() {
		if(isset($_SESSION[self::STATE])) {
			return $_SESSION[self::STATE];
		}
	}

	// Uppdatera användaren i Sessionen
	public static function updateLoggedInUser(\model\User $user) {
		$_SESSION[self::STATE] = $user;
	}

	public function getToken() {
		return $this->m_token;
	}

	// Funktion för att logga in, kollar mot Databasen
	public function doLogin($username, $password) {
		$e = new \Common\Encrypt();
		$pw = $e->encryptPassword($password);

		$user = $this->m_userDal->selectUserBy(array(\DBAccess\UserDAL::COL_USER_NAME, \DBAccess\UserDAL::COL_USER_PASSWORD),
												array($username, $pw), 
												array(\DBAccess\UserDAL::COL_USER_NAME_TYPE, \DBAccess\UserDAL::COL_USER_PASSWORD_TYPE));

		if($this->setState($user)) {
			return true;
		}

		return false;
	}

	// Logga in från kaka
	public function doCookieLogin($token) {
		$user = $this->m_userDal->selectUserBy(array(\DBACcess\UserDAL::COL_USER_TOKEN), array($token), array(\DBACcess\UserDAL::COL_USER_TOKEN_TYPE));

		if($this->setState($user)) {
			return true;
		}
	}

	// Sätt login till true om allt stämmer
	public function setState($user) {
		if($user !== false) {
			// Är användaren aktiverad?
			if($user->getActive() == \model\User::ACTIVE) {
				$_SESSION[self::STATE] = $user;

				// Hämtar ut cookie-token
				if($user->getToken() != null) {
					$this->m_token = $user->getToken();
				}

				return true;
			} 
		}
		return false;
	}

	// Funktion för att logga ut
	public static function doLogout() {
		unset($_SESSION[self::STATE]);
	}

}

