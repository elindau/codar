<?php

namespace model;

class MainHandler {
	const ERROR_NO_CODAR = 0;

	private $m_codarDal = null;

	public function __construct(\DBAccess\Database $db) {
		$this->m_codarDal = new \DBAccess\CodarDAL($db);
	}

	public function getCodar($codarId) {
		$codar = $this->m_codarDal->selectCodarBy(array(\DBAccess\CodarDAL::COL_CODAR_ID), array($codarId), array(\DBAccess\CodarDAL::COL_CODAR_ID_TYPE));
		return $codar == false ? self::ERROR_NO_CODAR : $codar;
	}

	public function getTopCodars() {
		$codars = $this->m_codarDal->selectAllCodars();
		
		// Sortera på level
		usort($codars, function($a, $b) {
			return $a->getCodarLevel() < $b->getCodarLevel();
		});

		// Retunera bara de fem översta
		$topCodars = $codars;
		if(count($codars) > 5) {
			$topCodars = array();
			for($i = 0; $i < 5; $i++) {
				$topCodars[] = $codars[$i];
			}
		}

		return $topCodars;
	}
}