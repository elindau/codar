<?php

namespace model;

class Training {


	private $m_trainingId = 0;
	private $m_name = null;
	private $m_value = 0;
	private $m_timelock = 0;

	public function __construct($tId, $name, $value, $timelock) {
		$this->setTrainingId($tId);
		$this->setName($name);
		$this->setValue($value);
		$this->setTimelock($timelock);
	}

	public function setTrainingId($trainingId) {
		$this->m_trainingId = $trainingId;
	}

	public function getTrainingId() {
		return $this->m_trainingId;
	}

	public function setName($name) {
		$this->m_name = $name;
	}

	public function getName() {
		return $this->m_name;
	}

	public function setValue($value) {
		$this->m_value = $value;
	}

	public function getValue() {
		return $this->m_value;
	}

	public function setTimelock($timelock) {
		$this->m_timelock = $timelock;
	}

	public function getTimelock() {
		return $this->m_timelock;
	}

	public function getCost() {
		return round(($this->m_value * 15), 0);
	}
}