<?php

namespace model;

class ValidMailHandler {

	const ERROR_ALREADY_ACTIVE = 0;
	const ERROR_WRONG_ID = 1;
	const ERROR_COULDNT_ACTIVATE = 2;

	private $m_userDal = null;

	public function __construct($db) {
		
		$this->m_userDal = new \DBAccess\UserDAL($db);
	}

	public function isNonActive($mailId) {
		$error = null;

		// Finns mailIdt?
		$user = $this->m_userDal->selectUserBy(array(\DBAccess\UserDAL::COL_USER_EMAILA),
												array($mailId), 
												array(\DBAccess\UserDAL::COL_USER_EMAILA_TYPE));

		// Om träff och inaktiv user, retunera user.. 
		if($user != false) {
			if($user->getActive() == \model\User::INACTIVE) {
				return $user;
			}
			$error = self::ERROR_ALREADY_ACTIVE;	
		} else {
			$error = self::ERROR_WRONG_ID;
		}
		

		// .. annars fel
		return $error;
	}

	// Aktivera användaren
	public function activateUser(\model\User $user) {
		return $this->m_userDal->updateUser($user, array(\DBAccess\UserDAL::COL_USER_ACTIVE),
													array(\model\User::ACTIVE), 
													array(\DBAccess\UserDAL::COL_USER_ACTIVE_TYPE));
	}
}