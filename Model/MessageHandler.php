<?php

namespace model;

class MessageHandler {

	private $m_messageDal = null;
	private $m_codarDal = null;

	public function __construct(\DBAccess\Database $db) {
		$this->m_messageDal = new \DBAccess\MessageDAL($db);
		$this->m_codarDal = new \DBAccess\CodarDAL($db);
	}

	// Hämta ut alla meddelanden för din codar
	public function getAllMessages() {
		$codar = $this->getCodar();
		return $this->m_messageDal->selectMessages($codar->getCodarId());
	}

	public function deleteMessage($messageId) {
		return $this->m_messageDal->deleteMessage($messageId);
	}

	public static function addMessage(\model\Message $message, \DBAccess\Database $db) {
		$messageDal = new \DBAccess\MessageDAL($db);
		$messageDal->insertMessage($message);
	}

	public function deleteAllMessages() {
		$codar = $this->getCodar();
		$messages = array();
		$messages = $this->m_messageDal->selectMessages($codar->getCodarId());

		foreach($messages as $message) {
			$this->m_messageDal->deleteMessage($message->getMessageId());
		}
	}

	private function getCodar() {

		// Hämta ut användare
		$user = \model\LoginHandler::getLoggedInUser();
		// Hämta ut Codar
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_ID);
		$types = array(\DBAccess\CodarDAL::COL_CODAR_ID_TYPE);
		$equals = array($user->getCodarId());
		$codar = $this->m_codarDal->selectCodarBy($arguments, $equals, $types);

		return $codar;
	}
}