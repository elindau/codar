<?php

namespace model;

class TrainingHandler {

	const ERROR_LOCKED = 0;
	const ERROR_CANT_AFFORD = 1;
	const ERROR_NO_ENERGY = 2;

	private $m_trainingDal = null;
	private $m_codarDal = null;
	private $m_lock = null;
	private $m_db = null;

	public function __construct(\DBAccess\Database $db) {
		$this->m_trainingDal = new \DBAccess\TrainingDAL($db);
		$this->m_codarDal = new \DBAccess\CodarDAL($db);
		$this->m_lock = new \model\Lock($db);
		$this->m_db = $db;
	}

	public function getAllAvailableTrainingTypes() {
		$user = \model\LoginHandler::getLoggedInUser();
		$trainingTypes = $this->m_trainingDal->selectAllAvailableTraining($user->getCodarId());
		return $trainingTypes;
	}

	/**
	 * Hämta ut alla träningstyper som användaren inte redan vet
	 * @return array \model\Training
	 */
	public function getAllTrainingTypes() {
		$buyTraining = $this->m_trainingDal->selectAllTraining();
		// Träning som redan finns
		$alreadyKnown = $this->getAllAvailableTrainingTypes();
		$deleteTrainingPos = array();

		foreach($alreadyKnown as $known) {
			for($i = 0; $i < count($buyTraining); $i++) {
				if($buyTraining[$i]->getTrainingId() == $known->getTrainingId()) {
					// Spara unden positionen
					$deleteTrainingPos[] = $i;
				}
			}
		}

		// Ta bort från arrayen
		foreach($deleteTrainingPos as $pos) {
			unset($buyTraining[$pos]);
		}
		
		return $buyTraining;
	}

	/**
	 * Skickar en codar på träning och låser den
	 * @param int $trainingId 
	 * @return true
	 */
	public function sendCodarTraining($trainingId) {
		$codar = $this->getCodar();

		// Är codarn låst?
		if($this->m_lock->isLocked($codar->getCodarId()) == false) {
			$timeWhenLock = microtime(true);

			// Lås Codar
			$codar->setCodarTrainingId($trainingId);
			// .. måste lägga till funktion för timeWhenLock och isLocked på codar
			$codar->setTimeWhenLocked($timeWhenLock);
			$this->m_lock->lockCodar($codar);
			
			return true;
		} else {
			// Har codarn för lite energi?
			if($codar->getEnergy() <= \model\Codar::MIN_ENERGY) {
				return self::ERROR_NO_ENERGY;
			}

			return self::ERROR_LOCKED;
		}
	}

	/**
	 * Lägg till träning till en Codar
	 * @param int $trainingId 
	 * @return boolean
	 */
	public function buyTraining($trainingId) {
		$codar = $this->getCodar();

		// Hämta ut Träningen
		$arguments = array(\DBAccess\TrainingDAL::COL_TRAINING_ID);
		$types = array(\DBAccess\TrainingDAL::COL_TRAINING_ID_TYPE);
		$equals = array($trainingId);
		$training = $this->m_trainingDal->selectTrainingBy($arguments, $equals, $types);

		// Har användaren råd
		if($codar->getMoney() >= $training->getCost()) {
			// Lägg till träning
			$outcome = $this->m_trainingDal->insertAvailableTraining($trainingId, $codar->getCodarId());
			if($outcome) {
				// Ta bort pengar
				$level = new \model\Level($codar, $this->m_db);
				$level->removeMoneyFromCodar($codar->getMoney() - $training->getCost());
				return true;
			} else {
				trigger_error(\Common\Strings::e_database);
			}

		} else {
			return self::ERROR_CANT_AFFORD;
		}
		
	}

	private function getCodar() {
		// Hämta ut användare
		$user = \model\LoginHandler::getLoggedInUser();
		// Hämta ut Codar
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_ID);
		$types = array(\DBAccess\CodarDAL::COL_CODAR_ID_TYPE);
		$equals = array($user->getCodarId());
		$codar = $this->m_codarDal->selectCodarBy($arguments, $equals, $types);

		return $codar;
	}
}