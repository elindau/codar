<?php
namespace model;

// Mellanhand till UserDAL
class AdminH {

	private $m_userDal = null;
	private $m_db = null;

	public function __construct($db) {
		$this->m_userDal = new \DBAccess\UserDAL($db);
		$this->m_db = $db;
	}

	public function getUsers() {

		// Lägg till alla användare förutom din egen,
		// inte heller andra admins
		$allUsers = array();
		$users = $this->m_userDal->selectAllUsers();
		foreach($users as $user) {
			if($user->getUserId() == \model\LoginHandler::getLoggedInUser()->getUserId() || 
				$user->getRole() == \model\User::ADMIN) {
				// Gör ingenting
			} else {
				// Lägg till i array
				$allUsers[] = $user;
			}
		}
		return $allUsers;
	}

	public function getUser($userId) {

		$arguments = array(\DBAccess\UserDAL::COL_USER_ID);
		$equals = array($userId);
		$types = array(\DBAccess\UserDAL::COL_USER_ID_TYPE);
		return $this->m_userDal->selectUserBy($arguments, $equals, $types);
	}

	// Ta bort användare
	public function deleteUser($userId) {
		return $this->m_userDal->deleteUser($userId);
	}

	// Uppdatera användare
	public function updateUser($id, $username, $pw, $pwrepeat, $email, $active) {
		$errors = array();

		if($pw !== $pwrepeat) {
			$errors[] = \Common\Strings::e_pwMatch;
		}

		if(!(\Common\Validation::validatePassword($pw))) {
			$errors[] = \Common\Strings::e_pwFormat;
		}

		if($username !== \Common\Validation::filterHTML($username)) {
			$errors[] = \Common\Strings::e_htmlFormat;
		}

		if(!(\Common\Validation::validateName($username))) {
			$errors[] = \Common\Strings::e_usernameFormat;
		}
		
		if(!(\Common\Validation::validateEmail($email))) {
			$errors[] = \Common\Strings::e_emailFormat;
		}

		// Finns redan användarnamnet?
		$user = $this->m_userDal->selectUserBy(array(\DBAccess\UserDAL::COL_USER_NAME),
												array($username), 
												array(\DBAccess\UserDAL::COL_USER_NAME_TYPE));
		if($user !== false && $user->getUserId() != $id) {
			$errors[] = \Common\Strings::e_usernameExists;
		}

		// Finns redan mailen?
		$user = $this->m_userDal->selectUserBy(array(\DBAccess\UserDAL::COL_USER_EMAIL), array($email), array(\DBAccess\UserDAL::COL_USER_EMAIL_TYPE));
		if($user !== false && $user->getUserId() != $id) {
			$errors[] = \Common\Strings::e_mailExists;
		}
		
		// Retunera ev. fel
		if(count($errors) != 0) {
			return $errors;
		}

		// Skapa användare
		$user = new \model\User($id, $username, $pw, $email);

		$arguments = array(\DBAccess\UserDAL::COL_USER_NAME, \DBAccess\UserDAL::COL_USER_PASSWORD, \DBAccess\UserDAL::COL_USER_EMAIL, \DBAccess\UserDAL::COL_USER_ACTIVE);
		$equals = array($username, $pw, $email, $active);
		$types = array(\DBAccess\UserDAL::COL_USER_NAME_TYPE, \DBAccess\UserDAL::COL_USER_PASSWORD_TYPE, \DBAccess\UserDAL::COL_USER_EMAIL_TYPE, \DBAccess\UserDAL::COL_USER_ACTIVE_TYPE);
		return $this->m_userDal->updateUser($user, $arguments, $equals, $types);
	}

	// Lägg till användare med RegisterUser-model
	public function addUser($username, $pw, $pwrepeat, $email) {
		$registerUser = new \model\RegisterUserH($this->m_db);
		return $registerUser->registerUser($username, $pw, $pwrepeat, $email);
	}
}