<?php

namespace model;

class Message {

	const SOURCE = "Common/img/message/";
	const GOT_MESSAGE = "got_message.png";
	const NO_MESSAGE = "no_message.png";

	private $m_messageId = 0;
	private $m_messageCodarId = 0;
	private $m_message = null;

	public function __construct($mId, $mcId, $msg) {
		$this->setMessageId($mId);
		$this->setMessageCodarId($mcId);
		$this->setMessage($msg);
	}

	public function setMessageId($messageId) {
		$this->m_messageId = $messageId;
	}

	public function getMessageId() {
		return $this->m_messageId;
	}

	public function setMessageCodarId($messageCodarId) {
		$this->m_messageCodarId = $messageCodarId;
	}

	public function getMessageCodarId() {
		return $this->m_messageCodarId;
	}

	public function setMessage($message) {
		$this->m_message = $message;
	}

	public function getMessage() {
		return $this->m_message;
	}
}