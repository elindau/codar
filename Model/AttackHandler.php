<?php

namespace model;

class AttackHandler {

	const MATCH_WON_VALUE = 2;
	const WON_LEVEL_RATE = 2.5;
	const MATCH_MONEY_VALUE = 100;
	const MATCH_LOST_VALUE = 50;
	const LOST_LEVEL_RATE = 1.5;
	const MONEY_RATE = 0.2;
	const MONEY_RATE_LOST = 0.1;

	const WON = 1;
	const LOST = 2;
	const ERROR_NO_CODAR = 3;
	const ERROR_LOCKED = 4;
	const ERROR_NO_ENERGY = 5;

	private $m_codarDal = null;
	private $m_earnedExp = 0;
	private $m_earnedMoney = 0;
	private $m_lostMoney = 0;
	private $m_db = null;

	public function __construct(\DBAccess\Database $db) {
		$this->m_codarDal = new \DBAccess\CodarDAL($db);
		$this->m_db = $db;
	}

	// Hämta ut tjänad xp
	public function getEarnedExp() {
		return $this->m_earnedExp;
	}

	public function getEarnedMoney() {
		return $this->m_earnedMoney;
	}

	public function getLostMoney(){
		return $this->m_lostMoney;
	}

	/**
	* Hämta ut alla codars förutom användarens
	*
	* @return array $codars \model\Codar
	*/
	public function getAllCodarsExceptUsers() {

		$user = \model\LoginHandler::getLoggedInUser();

		// Hämta ut alla Codars..
		$dbCodars = $this->m_codarDal->selectAllCodars();

		// .. förutom din egen
		$codars = array();
		foreach($dbCodars as $cdr) {
			if($cdr->getCodarId() != $user->getCodarId()) {
				$codars[] = $cdr;
			}
		}
		return $codars;
	}

	/**
	* Attackera annan codar
	*
	* @param $codarId
	* @return int (won = 1/lost = 2/error > 2)
	*/
	public function attackCodar($codarId) {

		// Hämta ut den codar som attackeras
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_ID);
		$equals = array($codarId);
		$types = array(\DBAccess\CodarDAL::COL_CODAR_ID_TYPE);
		$enemyCodar = $this->m_codarDal->selectCodarBy($arguments, $equals, $types);

		// Hämta ut 'din' codar
		$user = \model\LoginHandler::getLoggedInUser();
		$equals = array($user->getCodarId());
		$myCodar = $this->m_codarDal->selectCodarBy($arguments, $equals, $types);

		// Är codarn låst?
		$lock = new \model\Lock($this->m_db);
		if($lock->isLocked($myCodar->getCodarId())) {

			// Har codarn för lite energi?
			if($myCodar->getEnergy() <= \model\Codar::MIN_ENERGY) {
				return self::ERROR_NO_ENERGY;
			}
			
			return self::ERROR_LOCKED;
		}

		// Kör match
		if($enemyCodar != false && $myCodar != false) {
			// Vänta lite innan return
			// usleep(1000000);
			return $this->codarMatch($enemyCodar, $myCodar);
		} else {
			// Fel vid select
			return self::ERROR_NO_CODAR;
		}
	}

	/**
	 * Kör match mellan två Codar's
	 * @param type \model\Codar $enemy 
	 * @param type \model\Codar $hero 
	 * @return int
	 */
	private function codarMatch(\model\Codar $enemy, \model\Codar $hero) {
		$tied = true;
		$errors = array();
		$outcome = null;
		$eWon = 0;
		$hWon = 0;

		while($tied) {

			// Kör ett par gånger för mer rätt resultat
			for($i = 0; $i < 5; $i++) {
				// Hämta ut ett värde beroende på slump samt level
				$enemySeed = ($enemy->getCodarLevel() + 1) * mt_rand(1, 2);
				$heroSeed = ($hero->getCodarLevel() + 1) * mt_rand(1, 2);

				// Få ut match-värdet
				$enemyValue = mt_rand(0, $enemySeed);
				$heroValue = mt_rand(0, $heroSeed);

				if($enemyValue > $heroValue) {
					$eWon++;
				} else if($heroValue > $enemyValue) {
					$hWon++;
				}
			}


			if($eWon > $hWon) {
				// Fiende vann
				$this->gainWonMatch($enemy, $hero->getCodarLevel(), $enemy->getCodarLevel());
				$cash = $this->lostMatch($hero, $hero->getCodarLevel(), $enemy->getCodarLevel());

				// Skicka meddelande till 'fiende' om vinst
				$heroName = $hero->getCodarName();
				\model\MessageHandler::addMessage(new \model\Message(0, $enemy->getCodarId(), "Du blev attackerad och vann mot $heroName"), $this->m_db);

				// Inga pengar!
				if($cash === false) {
					// Använda level-klassen för att deLevla codarn
					$level = new \model\Level($hero, $this->m_db);
					$level->deLevelCodar($hero);
				}

				$outcome = self::LOST;

				$tied = false;

			} else if($hWon > $eWon) {
				// 'Du' vann
				$this->gainWonMatch($hero, $hero->getCodarLevel(), $enemy->getCodarLevel());
				$cash = $this->lostMatch($enemy, $hero->getCodarLevel(), $enemy->getCodarLevel());

				// Skicka meddelande till 'fiende' om förlust
				$heroName = $hero->getCodarName();
				\model\MessageHandler::addMessage(new \model\Message(0, $enemy->getCodarId(), "Du blev attackerad och förlorade mot $heroName"), $this->m_db);
				
				// Inga pengar!
				if($cash === false) {
					// Använda level-klassen för att deLevla codarn
					$level = new \model\Level($enemy, $this->m_db);
					$level->deLevelCodar($enemy);
				}

				$outcome = self::WON;

				$tied = false;

			} else {
				// Lika
				$tied = true;
			}
		}

		// Ta bort energi från attackerande Codar
		$lock = new \model\Lock($this->m_db);
		$lock->energyHandler($hero, $hero->getEnergy()-1);

		return $outcome;
	}

	/**
	 * Lägger till xp och pengar till den Codar som vann
	 * @param type \model\Codar $codar 
	 * @param int $lvl Generera xp beroende på användarens lvl och inte codarns  
	 * @return int $expEarned
	 */
	private function gainWonMatch(\model\Codar $codar, $heroLvl, $enemyLvl) {
		// Skillnaden i level
		$diff = $heroLvl - $enemyLvl;
		$lvlDiffRate = 1;

		if($diff >= 10) {
			$lvlDiffRate = 0.0;
		} else if($diff <= -10) {
			$lvlDiffRate = 2.0;
		} else if($diff >= 5) {
			$lvlDiffRate = 0.1;
		} else if($diff <= -5) {
			$lvlDiffRate = 1.3;
		} else {
			$lvlDiffRate = 1;
		}

		// Algoritm för hur mycket exp man får
		$expEarned = (($heroLvl + 1 * self::WON_LEVEL_RATE) * self::MATCH_WON_VALUE) * $lvlDiffRate;
		$expEarned = round($expEarned, 0);

		// Algoritm för pengar
		$moneyEarned = ((($heroLvl + 1 * self::WON_LEVEL_RATE) * self::MATCH_MONEY_VALUE) * self::MONEY_RATE_LOST) * $lvlDiffRate;
		$moneyEarned = round($moneyEarned, 0);

		// Spara undan
		$this->m_earnedExp = $expEarned;
		$this->m_earnedMoney = $moneyEarned;

		// Lägg till
		$exp = $expEarned + $codar->getCodarExp();
		$money = $moneyEarned + $codar->getMoney();

		// Använda level-klassen för att lägga till exp och pengar
		$level = new \model\Level($codar, $this->m_db);
		$level->addExpToCodar($exp);
		$level->addMoneyToCodar($money);
		
		return $expEarned;

	}

	private function lostMatch(\model\Codar $codar, $heroLvl, $enemyLvl) {

		// Skillnaden i level
		$diff = $heroLvl - $enemyLvl;
		$lvlDiffRate = 1;

		if($diff >= 10) {
			$lvlDiffRate = 2.0;
		} else if($diff <= -10) {
			$lvlDiffRate = 0.1;
		} else if($diff >= 5) {
			$lvlDiffRate = 1.0;
		} else if($diff <= -5) {
			$lvlDiffRate = 0.2;
		} else {
			$lvlDiffRate = 0.8;
		}

		// Algoritm för pengar
		$moneyLost = ((($heroLvl + 1 * self::LOST_LEVEL_RATE) * self::MATCH_LOST_VALUE) * self::MONEY_RATE) * $lvlDiffRate;
		$moneyLost = round($moneyLost, 0);

		// Spara undan
		$this->m_lostMoney = $moneyLost;
		$money = $codar->getMoney() - $moneyLost;

		// Använda level-klassen för att lägga till exp och pengar
		$level = new \model\Level($codar, $this->m_db);
		return $level->removeMoneyFromCodar($money);
	}
}