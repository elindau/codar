<?php
namespace model;

class RegisterUserH {

	private $m_userDal = null;

	public function __construct($db) {
		$this->m_userDal = new \DBAccess\UserDAL($db);
	}

	public function registerUser($username, $pw, $pwrepeat, $email) {
		$errors = array();

		if($pw !== $pwrepeat) {
			$errors[] = \Common\Strings::e_pwMatch;
		}

		if(!(\Common\Validation::validatePassword($pw))) {
			$errors[] = \Common\Strings::e_pwFormat;
		}

		// Kasta fel här istället?
		if($username !== \Common\Validation::filterHTML($username)) {
			$errors[] = \Common\Strings::e_htmlFormat;
		}

		if(!(\Common\Validation::validateName($username))) {
			$errors[] = \Common\Strings::e_usernameFormat;
		}
		
		if(!(\Common\Validation::validateEmail($email))) {
			$errors[] = \Common\Strings::e_emailFormat;
		}

		// Finns redan användarnamnet?
		$user = $this->m_userDal->selectUserBy(array(\DBAccess\UserDAL::COL_USER_NAME),
												array($username), 
												array(\DBAccess\UserDAL::COL_USER_NAME_TYPE));
		if($user !== false) {
			$errors[] = \Common\Strings::e_usernameExists;
		}

		// Finns redan mailen?
		$user = $this->m_userDal->selectUserBy(array(\DBAccess\UserDAL::COL_USER_EMAIL), array($email), array(\DBAccess\UserDAL::COL_USER_EMAIL_TYPE));
		if($user !== false) {
			$errors[] = \Common\Strings::e_mailExists;
		}
		
		// Retunera ev. fel
		if(count($errors) != 0) {
			return $errors;
		}

		// Generera en random-token till remember-me funktionen
		$randToken = hash('sha256', uniqid(mt_rand(), true).uniqid(mt_rand(), true));

		// Skapa en ny användare..
		$user = new \model\User(0, $username, $pw, $email);
		// .. med random cookie-token ..
		$user->setToken($randToken);
		// .. och en aktivera mail address.
		$mailId = $this->sendMail($user);
		// Lyckades mailet skickas
		if($mailId != false)
			$user->setMailId($mailId);

		return $this->m_userDal->insertUser($user);
	}

	private function sendMail($user) {

		// Generera en random-sträng till mail funktion
        $to = $user->getEmail(); // Ska vara användarens mail så småningom
        $mailId = hash('sha256', uniqid(mt_rand(), true));
        $subject = "Welcome to Codar!";
        $link = "http://codar.se" . \view\Navigation::generateMailGet() . $mailId;
        $header = "Activate your Codar-account!";
        $message = "Activate your Codar-account, press this link $link";
        $sent = mail($to, $subject, $message, $header);
        if($sent) {
           	return $mailId;
        }
        return false;
	}

	private function sendMailLocal() {

		// Generera en random-sträng till mail funktion
        $to = \Common\Strings::ADMIN_MAIL; // Ska vara användarens mail så småningom
        $mailId = hash('sha256', uniqid(mt_rand(), true));
        $subject = "test";
        $link = "http://localhost/codar/" . \view\Navigation::generateMailGet() . $mailId;
        $header = "Activate your Codar-account!";
        $message = "Activate your Codar-account, press this link $link";
        $sent = mail($to, $subject, $message, $header);
        if($sent) {
           	return $mailId;
        }
        return false;
	}
}