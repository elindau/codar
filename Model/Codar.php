<?php

namespace model;

class Codar {

	const MAX_ENERGY = 5;
	const MIN_ENERGY = 0;

	private $m_codarId = 0;
	private $m_name = null;
	private $m_languageId = null;
	private $m_level = 0;
	private $m_exp = 0;
	private $m_trainingId = 0;
	private $m_timeWhenLocked = 0;
	private $m_money = 0;
	private $m_energy = self::MIN_ENERGY;

	public function getCodarId() {
		return $this->m_codarId;
	}

	public function setCodarId($id) {
		$this->m_codarId = $id;
	}

	public function getCodarName() {
		return $this->m_name;
	}

	public function setCodarName($name) {
		$this->m_name = $name;
	}

	public function getCodarLanguageId() {
		return $this->m_languageId;
	}

	public function setCodarLanguageId($languageId) {
		$this->m_languageId = $languageId;
	}

	public function getCodarLevel() {
		return $this->m_level;
	}

	public function setCodarLevel($level) {
		$this->m_level = $level;
	}

	public function getCodarExp() {
		return $this->m_exp;
	}

	public function setCodarExp($exp) {
		$this->m_exp = $exp;
	}

	public function setCodarTrainingId($tId) {
		$this->m_trainingId = $tId;
	}

	public function getCodarTrainingId() {
		return $this->m_trainingId;
	}

	public function setTimeWhenLocked($twl) {
		$this->m_timeWhenLocked = $twl;
	}

	public function getTimeWhenLocked() {
		return $this->m_timeWhenLocked;
	}

	public function setMoney($money) {
		$this->m_money = $money;
	}

	public function getMoney() {
		return $this->m_money;
	}

	public function setEnergy($energy) {
		if($energy > self::MAX_ENERGY) {
			$energy = self::MAX_ENERGY;
		}
		$this->m_energy = $energy;
	}

	public function getEnergy() {
		return $this->m_energy;
	}

	public function getCodarImageName() {
		// Codars bild
		$imgLvl = 1;
		if($this->getCodarLevel() <= 10) {
			$imgLvl = 1;
		} else if($this->getCodarLevel() > 10 && $this->getCodarLevel() < 20) {
			$imgLvl = 2;
		} else {
			$imgLvl = 3;
		}

		return $imgLvl;
	}


	public function __construct($cId, $name, $level, $exp, $langId, $trainingId, $timeLocked, $money, $energy) {

		$this->setCodarId($cId);
		$this->setCodarName($name);
		$this->setCodarLanguageId($langId);
		$this->setCodarLevel($level);
		$this->setCodarExp($exp);
		$this->setCodarTrainingId($trainingId);
		$this->setTimeWhenLocked($timeLocked);
		$this->setMoney($money);
		$this->setEnergy($energy);
	}
}