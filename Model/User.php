<?php
namespace model;

class User {

	const ADMIN = 1;
	const USER = 0;
	const ACTIVE = 1;
	const INACTIVE = 0;
	const FIRST_LOGIN = 0;
	
	private $m_username = null;
	private $m_password = null;
	private $m_uId = 0;
	private $m_email = null;
	private $m_encrypt = null;
	private $m_role = null;
	private $m_token = null;
	private $m_mailId = null;
	private $m_active = 0;
	private $m_codarId = 0;

	public function setUsername($username) {
		$this->m_username = $username;
	}

	public function getUsername() {
		return $this->m_username;
	}

	// Hantera krypteringen här?
	public function setPassword($password, $crypt = true) {
		if($crypt) {
			$encrypt = new \Common\Encrypt();
			$this->m_password = $encrypt->encryptPassword($password);
		} else {
			$this->m_password = $password;
		}
		
	}

	public function getPassword() {
		return $this->m_password;
	}

	public function getEmail() {
		return $this->m_email;
	}

	public function setEmail($email) {
		$this->m_email = $email;
	}

	public function setUserId($id) {
		$this->m_uId = $id;
	}

	public function getUserId() {
		return $this->m_uId;
	}

	public function setRole($role) {
		$this->m_role = $role;
	}

	public function getRole() {
		return $this->m_role;
	}

	public function setToken($token) {
		$this->m_token = $token;
	}

	public function getToken() {
		return $this->m_token;
	}

	// Aktivera mail
	public function setMailId($mId) {
		$this->m_mailId = $mId;
	}

	public function getMailId() {
		return $this->m_mailId;
	}

	// Aktiv
	public function setActive($const) {
		$this->m_active = $const;
	}

	public function getActive() {
		return $this->m_active;
	}

	// Codar
	public function setCodarId($cId) {
		$this->m_codarId = $cId;
	}

	public function getCodarId() {
		return $this->m_codarId;
	}

	public function __construct($uId, $user, $pw, $email, $crypt = true) {
		$this->setUsername($user);
		$this->setPassword($pw, $crypt);
		$this->setEmail($email);
		$this->setRole(self::USER);
		$this->m_uId = $uId;
	}
}