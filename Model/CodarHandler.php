<?php

namespace model;

class CodarHandler {

	const ERROR_FAILED_TO_INSERT = 0;
	const ERROR_VALIDATION = 1;

	private $m_codarDal = null;
	private $m_userDal = null;
	private $m_trainingDal = null;
	private $m_codarId = 0;

	public function __construct(\DBAccess\Database $db) {
		$this->m_codarDal = new \DBAccess\CodarDAL($db);
		$this->m_userDal = new \DBAccess\UserDAL($db);
		$this->m_trainingDal = new \DBAccess\TrainingDAL($db);
	}

	public function getAllLanguages() {
		return $this->m_codarDal->selectAllLanguages();
	}

	public function getInsertedCodarId() {
		return $this->m_codarId;
	}

	public function registerCodar($name, $languageId) {
		// Validera namn
		if(\Common\Validation::validateName($name)) {

			// Insert codar
			$codar = new \model\Codar(0, $name, 0, 0, $languageId, 0, 0, \model\Money::STARTING_MONEY, \model\Codar::MAX_ENERGY);
			$codarId = $this->m_codarDal->insertCodar($codar);

			// Lägg till basträning
			$this->m_trainingDal->insertStartupTraining($codarId);

			// Lyckades inte inserten?
			if($codarId == false) {
				return self::ERROR_FAILED_TO_INSERT;
			}

			// Uppdatera user med codar-idt (hämta user från sessionen)
			$user = \model\LoginHandler::getLoggedInUser();
			$arguments = array(\DBAccess\UserDAL::COL_USER_CODAR_ID);
			$equals = array($codarId);
			$types = array(\DBAccess\UserDAL::COL_USER_CODAR_ID_TYPE);

			if($this->m_userDal->updateUser($user, $arguments, $equals, $types)) {
				// Uppdatera Session-usern också
				$user->setCodarId($codarId);
				\model\LoginHandler::updateLoggedInUser($user);

				// Så man får tag i codarIdt för header reloc
				$this->m_codarId = $codarId;

				return true;
			} else {
				trigger_error(\Common\Strings::e_database);
			}

		} else {
			// Fel validering
			return self::ERROR_VALIDATION;
		}
	}

}