<?php

namespace model;

class ForgotPasswordHandler {

	const ERROR_NO_EMAIL = 0;
	const ERROR_FAILED_TO_SEND = 1;
	const ERROR_WRONG_EMAIL_FORMAT = 2;

	private $m_userDal = null;

	public function __construct(\DBAccess\Database $db) {
		
		$this->m_userDal = new \DBAccess\UserDAL($db);
	}

	public function sendEmail($email) {
		// TODO: Validera mailen
		$error = null;

		// Validera mailen
		if(\Common\Validation::validateEmail($email)) {
			// Hämta ut användare med given email
			$user = $this->m_userDal->selectUserBy(array(\DBAccess\UserDAL::COL_USER_EMAIL),
										array($email), 
										array(\DBAccess\UserDAL::COL_USER_EMAIL_TYPE));

			if($user !== false) {
				// Generera nytt lösenord till användare
				$pw = $this->generatePassword();
				// Spara undan utan kryptering
				$beforePw = $pw;
				// Kryptera lösenord
				$e = new \Common\Encrypt();
				$pw = $e->encryptPassword($pw);

				// Uppdatera databas
				$outcome = $this->m_userDal->updateUser($user, array(\DBAccess\UserDAL::COL_USER_PASSWORD),
															array($pw), 
															array(\DBAccess\UserDAL::COL_USER_PASSWORD_TYPE));
				// Skicka mail till användare
				if($outcome) {
					// Lyckades det att skicka
					if($this->sendMail($email, $beforePw)) {
						return true;
					} else {
						// Gick inte att skicka mail
						$error = self::ERROR_FAILED_TO_SEND;
					}
				} else {
					// Gick inte uppdatera databasen, något är fel med den
					trigger_error(\Common\Strings::e_database);
				}
			} else {
				// Finns ingen användare med den mailen
				$error = self::ERROR_NO_EMAIL;
			}
		} else {
			// Ingen giltlig mail
			$error = self::ERROR_WRONG_EMAIL_FORMAT;
		}

		return $error;		
	}

	private function generatePassword() {
		// Längd på lösen
		$length = 8;
		// Tillåtna tecken
		$allowed = "23456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$password = "";

		// Plocka en random från tillåtna och lägg till
		for($i = 0; $i < $length; $i++) {
			$add = substr($allowed, mt_rand(0, strlen($allowed) - 1), 1);
			$password .= $add;
		}

		return $password;
	}

	private function sendMail($email, $pw) {
        //$to = \Common\Strings::ADMIN_MAIL; // Ska vara $email-variabeln
        $to = $email; 
        $subject = "Nytt lösenord";
        $header = "Nytt lösenord från Codar!";
        $message = "Ditt nya lösenord till codar.se är $pw";
        $sent = mail($to, $subject, $message, $header);

        if($sent) {
           	return true;
        }
        return false;
	}
}