<?php

namespace model;

class Item {

	const SOURCE = "Common/img/shop/";
	const MAX_VALUE = 5;
	const MIN_VALUE = 1;

	private $m_id = 0;
	private $m_name = null;
	private $m_cost = 0;
	private $m_filename = null;
	private $m_descr = null;
	private $m_value = self::MIN_VALUE;

	public function setId($id) {
		$this->m_id = $id;
	}

	public function getId(){
		return $this->m_id;
	}

	public function setName($name) {
		$this->m_name = $name;
	}

	public function getName() {
		return $this->m_name;
	}

	public function setCost($cost) {
		$this->m_cost = $cost;
	}

	public function getCost() {
		return $this->m_cost;
	}

	public function setFilename($filename) {
		$this->m_filename = $filename;
	}

	public function getFileSource() {
		return self::SOURCE.$this->m_filename;
	}

	public function getFilename() {
		return $this->m_filename;
	}

	public function setDescr($descr) {
		$this->m_descr = $descr;
	}

	public function getDescr(){
		return $this->m_descr;
	}

	public function setValue($value) {
		if($value <= self::MAX_VALUE && $value >= self::MIN_VALUE) {
			$this->m_value = $value;
		} else {
			$this->m_value = self::MIN_VALUE;
		}
	}

	public function getValue() {
		return $this->m_value;
	}

	// Hämtar ut beskrivning på itemet
	public static function getDescrAccordingTo($value) {

		$descrArray = array("Återställer 1 energi", 
			"Återställer 2 energi", "Återställer 3 energi", "Återställer 4 energi", 
			"Återställer din Codars energi");

		for($i = 0; $i < count($descrArray); $i++) {
			if($i+1 == $value) {
				return $descrArray[$i];
			}
		}
	}

	public function __construct($iId, $iName, $iCost, $iFilename, $iDescr, $iValue){
		$this->setId($iId);
		$this->setName($iName);
		$this->setCost($iCost);
		$this->setFilename($iFilename);
		$this->setDescr($iDescr);
		$this->setValue($iValue);
	}
}