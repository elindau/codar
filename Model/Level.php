<?php

namespace model;

// En modell / observer till level vid overflow-exp
class Level {

	const MAX_LEVEL = 100;
	const BASE_EXP = 100;
	const RATE = 0.5;
	
	private $m_codar = null;
	private $m_codarDal = null;
	private $m_db = null;

	public function __construct(\model\Codar $codar, \DBAccess\Database $db) {
		$this->m_codar = $codar;
		$this->m_codarDal = new \DBAccess\CodarDAL($db);
		$this->m_db = $db;
	}

	/**
	 * Lägg till Exp till en Codar
	 * @param int $exp 
	 */
	public function addExpToCodar($exp) {

		// Öka på level om det har överskridit xp
		while($exp >= self::getCodarMaxExp($this->m_codar)) {
			// Lägg till överflödig xp
			$overflow = $exp - self::getCodarMaxExp($this->m_codar);

			// Öka level
			$this->m_codar->setCodarLevel($this->m_codar->getCodarLevel() + 1);

			// Skriv över exp
			$exp = $overflow;
		}

		// Uppdatera Codar'n
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_EXP, \DBAccess\CodarDAL::COL_CODAR_LEVEL);
		$equals = array($exp, $this->m_codar->getCodarLevel());
		$types = array(\DBAccess\CodarDAL::COL_CODAR_EXP_TYPE, \DBAccess\CodarDAL::COL_CODAR_LEVEL_TYPE);
		$outcome = $this->m_codarDal->updateCodar($this->m_codar, $arguments, $equals, $types);

		if(!($outcome)) {
			trigger_error(\Common\Strings::e_database);
		} 
	}

	/**
	 * Lägg till Pengar till Codar
	 * @param int $money 
	 * @return -
	 */
	public function addMoneyToCodar($money) {

		// Uppdatera Codar'n
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_MONEY);
		$equals = array($money);
		$types = array(\DBAccess\CodarDAL::COL_CODAR_MONEY_TYPE);
		$outcome = $this->m_codarDal->updateCodar($this->m_codar, $arguments, $equals, $types);

		if(!($outcome)) {
			trigger_error(\Common\Strings::e_database);
		} 
	}

	/**
	 * Ta bort pengar från codar
	 * @param int $money 
	 * @return false
	 */
	public function removeMoneyFromCodar($money) {

		// Blir pengarna mindre än 0?
		if($money <= 0) {
			return false;
		} else {
			// Uppdatera Codar'n
			$arguments = array(\DBAccess\CodarDAL::COL_CODAR_MONEY);
			$equals = array($money);
			$types = array(\DBAccess\CodarDAL::COL_CODAR_MONEY_TYPE);
			$outcome = $this->m_codarDal->updateCodar($this->m_codar, $arguments, $equals, $types);

			if(!($outcome)) {
				trigger_error(\Common\Strings::e_database);
			} 
		}
	}


	/**
	 * Ta bort en level från en Codar
	 * @param type \model\Codar $codar 
	 * @return type
	 */
	public function deLevelCodar(\model\Codar $codar) {

		$newLevel = $codar->getCodarLevel() - 1;
		if($newLevel < 0) {
			// Spelet förloras
			$this->gameLost($codar);
		} else {
			// Ge Codarn lite pengar!
			$money = (\model\Money::STARTING_MONEY) * 0.8;
			round($money, 0);

			// Uppdatera databasen
			$arguments = array(\DBAccess\CodarDAL::COL_CODAR_LEVEL, \DBAccess\CodarDAL::COL_CODAR_MONEY);
			$equals = array($newLevel, $money);
			$types = array(\DBAccess\CodarDAL::COL_CODAR_LEVEL_TYPE, \DBAccess\CodarDAL::COL_CODAR_MONEY_TYPE);
			$outcome = $this->m_codarDal->updateCodar($codar, $arguments, $equals, $types);
		}
	}

	/**
	 * Level har underskridit 0 och spelet förloras
	 * @param type \model\Codar $codar 
	 * @return type
	 */
	private function gameLost(\model\Codar $codar) {

		// Hämta ut användare
		$userDal = new \DBAccess\UserDAL($this->m_db);
		$arguments = array(\DBAccess\UserDAL::COL_USER_CODAR_ID);
		$equals = array($codar->getCodarId());
		$types = array(\DBAccess\UserDAL::COL_USER_CODAR_ID_TYPE);
		$user = $userDal->selectUserBy($arguments, $equals, $types);

		// Ta bort codarId från användare
		$equals = array(0);
		$userDal->updateUser($user, $arguments, $equals, $types);

		// Ta bort från träning
		$trainingDal = new \DBAccess\TrainingDAL($this->m_db);
		$trainingDal->deleteAllFromAtraining($codar->getCodarId());
		

		// Ta bort codar
		$this->m_codarDal->deleteCodar($codar->getCodarId());

		// Logga ut användaren
		\model\LoginHandler::doLogout();

		// Header redirect
		header ("Location: http://{$_SERVER['HTTP_HOST']}/". \Common\Strings::GENERIC_LOST_PAGE ); 
	  	exit; 
	}

	/**
	 * Algoritm för att hantera xp för level
	 * @param int $level 
	 * @return int
	 */
	private static function generateExp($level) {
		$exp = (($level + 1) * self::BASE_EXP) * self::RATE;
		return $exp;
	}

	public static function getCodarMaxExp(\model\Codar $codar) {
		return self::generateExp($codar->getCodarLevel());
	}
}