<?php

namespace model;

class ShopHandler {

	const ERROR_CANT_AFFORD = 9;
	const ERROR_FULL_ENERGY = 10;
	const ERROR_HTTP = 11;
	const ERROR_FILE_SIZE = 12;
	const ERROR_EXT = 13;
	const ERROR_TYPE = 14;
	const ERROR_MOVE = 15;
	const ERROR_NAME = 16;
	const ERROR_NUMBER = 17;

	const ERROR_KEY = "error";
	const NAME_KEY = "name";
	const TYPE_KEY = "type";
	const TMP_NAME_KEY = "tmp_name";
	const SIZE = "size";

	private $ALLOWED_EXT = array("png");
	private $ALLOWED_TYPES = array("image/png");
	const MAX_FILE_SIZE = 10000;

	private $m_itemDal = null;
	private $m_lock = null;
	private $m_codarDal = null;
	private $m_db = null;

	public function __construct(\DBAccess\Database $db) {
		$this->m_itemDal = new \DBAccess\ItemDAL($db);
		$this->m_codarDal = new \DBAccess\CodarDAL($db);
		$this->m_lock = new \model\Lock($db);
		$this->m_db = $db;
	}

	public function getAllItems() {
		return $this->m_itemDal->selectAllItems();
	}

	public function buyItem($itemId) {
		$codar = $this->getCodar();

		// Hämta ut Itemet
		$arguments = array(\DBAccess\ItemDAL::COL_ITEM_ID);
		$types = array(\DBAccess\ItemDAL::COL_ITEM_ID_TYPE);
		$equals = array($itemId);
		$item = $this->m_itemDal->selectItemBy($arguments, $equals, $types);

		// Har användaren redan full energi
		if($codar->getEnergy() == \model\Codar::MAX_ENERGY) {
			return self::ERROR_FULL_ENERGY;
		}

		// Har användaren råd
		if($codar->getMoney() >= $item->getCost()) {
			// Använd itemet
			$eValue = $item->getValue();
			if($codar->getEnergy() + $eValue >= \model\Codar::MAX_ENERGY) {
				$energy = \model\Codar::MAX_ENERGY;
			} else {
				$energy = $codar->getEnergy() + $eValue;
			}
			
			$this->m_lock->energyHandler($codar, $energy);

			// Ta bort pengar
			$level = new \model\Level($codar, $this->m_db);
			$level->removeMoneyFromCodar($codar->getMoney() - $item->getCost());

			return true;
		} else {
			return self::ERROR_CANT_AFFORD;
		}
	}

	public function deleteItem($itemId) {

		// Hämta ut Itemet så vi får tag i namnet
		$arguments = array(\DBAccess\ItemDAL::COL_ITEM_ID);
		$types = array(\DBAccess\ItemDAL::COL_ITEM_ID_TYPE);
		$equals = array($itemId);
		$item = $this->m_itemDal->selectItemBy($arguments, $equals, $types);
		$file = $item->getFileSource();

		// Ta bort bilden
		if (file_exists($file)) {
    		if(!(unlink($file))) {
				// Logga
				error_log("Kunde inte ta bort filen", 0);
			}
		} else {
    		//
    		error_log("Filen fanns inte", 0); 
		}

		// Ta bort från databasen hur som hur
		return $this->m_itemDal->deleteItem($itemId);
	}

	public function addItem($file, $name, $cost, $value) {
		$errors = array();
		$saveFolder = \model\Item::SOURCE;
		$allowedExt = $this->ALLOWED_EXT;
		$allowedTypes = $this->ALLOWED_TYPES;

		// Finns det några fel i filen?
		if($file[self::ERROR_KEY] > 0) {
			$errors[] = $file[self::ERROR_KEY];
		}

		// Explode delar upp filnamnet i en array, end ger filtypen, i det här fallet png.
		$fileName = $file[self::NAME_KEY];
		$fileExtension = end(explode(".", $fileName));

		// Kollar så filen är uppladdad från HTTP
		if(!(is_uploaded_file($file[self::TMP_NAME_KEY]))) {
			$errors[] = self::ERROR_HTTP;
		}

		// Kollar så filen inte är för stor
		if($file[self::SIZE] > self::MAX_FILE_SIZE) {
			$errors[] = self::ERROR_FILE_SIZE;
		}

		// Kollar så filen är av rätt typ
		if(!(in_array($file[self::TYPE_KEY], $allowedTypes))) {
			$errors[] = self::ERROR_TYPE;
		}

		// Kollar så filen har rätt ändelse
		if(!(in_array($fileExtension, $allowedExt))) {
			$errors[] = self::ERROR_EXT;
		}

		// Finns redan filnamnet? Körs till träff inte längre finns
		$files = $this->readFiles();
		$hit = 0;
		while($hit < 2) {
			foreach($files as $f) {
				$hit++;
				if(basename($f) == $file[self::NAME_KEY]) {
					$fileNameExt = explode(".", $file[self::NAME_KEY]);
					$fileNameExt[0] .= "_kopia";
					$file[self::NAME_KEY] = implode(".", $fileNameExt);
					$hit = 0;
				}
			}
		}

		// Validera namn
		if(!(\Common\Validation::validateName($name))) {
			$errors[] = self::ERROR_NAME;
		}

		// Validera kostnad
		if(!(\Common\Validation::validateNumber($cost))) {
			$errors[] = self::ERROR_NUMBER;
		}

		// Finns det några fel nu?
		if(count($errors) != 0) {
			return $errors;
		}	

		// Sätt descr med värde
		$descr = \model\Item::getDescrAccordingTo($value);
		$imageName = $file[self::NAME_KEY];

		// Vart filen ska sparas
		$saveFile = $saveFolder.$file[self::NAME_KEY];

		// Försök flytta och spara filen
		if(!(move_uploaded_file($file[self::TMP_NAME_KEY], $saveFile))) {
			$errors[] = self::ERROR_MOVE;
		}

		// Finns det några fel nu?
		if(count($errors) != 0) {
			return $errors;
		}	

		$item = new \model\Item(0, $name, $cost, $imageName, $descr, $value);

		// Lägg till i databasen
		$outcome = $this->m_itemDal->insertItem($item);

	}

	/**
	* Läs filer från map
	*
	* @return array med filer
	* @throws Exception om mappen inte finns
	*/
	private function readFiles() {
		$dir = \model\Item::SOURCE;
		$files = array();

		// Läs från mappen och spara undan filnamnen i en array
		if(is_dir($dir)) {
			if($dh = opendir($dir)) {
				while(($file = readdir($dh)) !== false) {
					// Så man inte får med själva mappen
					if(is_dir($file) == false) {
						$files[] = $dir.$file;
					}
				}
				closedir($dh);
			}
			return isset($files) ? $files : null;
		} else {
			return null;
		}
	}

	private function getCodar() {
		// Hämta ut användare
		$user = \model\LoginHandler::getLoggedInUser();
		// Hämta ut Codar
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_ID);
		$types = array(\DBAccess\CodarDAL::COL_CODAR_ID_TYPE);
		$equals = array($user->getCodarId());
		$codar = $this->m_codarDal->selectCodarBy($arguments, $equals, $types);

		return $codar;
	}
}