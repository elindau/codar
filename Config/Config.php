<?php

namespace config;

// Ställ in lite i .ini-filen
class Config {
	private $m_errorHandler = "\Error::myErrHandler";

	public function run() {
		// Felhantering
	  	error_reporting(E_ERROR);
		ini_set('log_errors', '1');
		ini_set('display_errors', '1');
		ini_set('error_log', 'my-errors.log');
		set_error_handler($this->m_errorHandler);			
	}
}
