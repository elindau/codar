-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Värd: localhost
-- Skapad: 20 okt 2012 kl 10:08
-- Serverversion: 5.5.16
-- PHP-version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databas: `codar`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `codar`
--

CREATE TABLE IF NOT EXISTS `codar` (
  `codarId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `level` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `languageId` int(11) NOT NULL,
  `trainingId` int(11) NOT NULL DEFAULT '0',
  `timeLocked` float DEFAULT NULL,
  PRIMARY KEY (`codarId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumpning av Data i tabell `codar`
--

INSERT INTO `codar` (`codarId`, `name`, `level`, `exp`, `languageId`, `trainingId`, `timeLocked`) VALUES
(4, 'flempe', 9, 64, 1, 0, 0),
(9, 'fff', 14, 2, 1, 1, 1350680000),
(10, 'lowCodar', 1, 99, 1, 0, 0),
(11, 'medCodar', 10, 102, 1, 0, 0),
(12, 'highCodar', 80, 63, 1, 0, 0);

-- --------------------------------------------------------

--
-- Tabellstruktur `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `languageId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`languageId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumpning av Data i tabell `language`
--

INSERT INTO `language` (`languageId`, `name`) VALUES
(1, 'PHP'),
(2, 'C#');

-- --------------------------------------------------------

--
-- Tabellstruktur `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `password` varchar(256) NOT NULL,
  `email` varchar(100) NOT NULL,
  `token` char(64) NOT NULL,
  `emailActive` char(64) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `role` int(11) NOT NULL DEFAULT '0',
  `codarId` int(11) NOT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumpning av Data i tabell `login`
--

INSERT INTO `login` (`userId`, `name`, `password`, `email`, `token`, `emailActive`, `active`, `role`, `codarId`) VALUES
(10, 'empe', '$2jas8Wr6/7Qk', 'empe@em.se', 'dd409d9c60f841fe6a2c0be11004eda7b8580915d8ec43fe40986f4991438ee4', '4b0c7499098aca901930f868802c26a985673eb0f73d9fe9bc7a29b07e6eaf68', 1, 0, 4),
(20, 'nisse', '$2jas8Wr6/7Qk', 'nisse@nisse.se', 'c31c82c5d9a977d58c24be02c5a0c76bd747c4b300c368ed56fe82b2bfd53cf8', 'af93ff2af507d609110777282000136c20e6674817df110f660530ad843181cb', 1, 0, 9);

-- --------------------------------------------------------

--
-- Tabellstruktur `training`
--

CREATE TABLE IF NOT EXISTS `training` (
  `trainingId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `value` int(11) NOT NULL,
  `timelock` int(11) NOT NULL,
  PRIMARY KEY (`trainingId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumpning av Data i tabell `training`
--

INSERT INTO `training` (`trainingId`, `name`, `value`, `timelock`) VALUES
(1, 'Föreläsning', 100, 30),
(2, 'Hackaton', 500, 240),
(3, 'Tutorial', 10, 10),
(4, 'Diskussion', 20, 20);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
