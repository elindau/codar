<?php
require_once('Common/Includes.php');
session_start();

class MasterInstaller extends \controller\BaseController {

	public static function doInstall() {
		// Kör config-filen
        $body = "";
        $page = new \Common\PageView();

        // Fånga databasfel
        try {
            $installView = new \view\InstallView();
            $install = new \controller\InstallController();
            $body = $install->doControll($installView);
        } catch(\Exception $e) {
            // Visa bara Controllers utan databas (finns inga just nu)
            $err = $e->GetMessage();
            $installView->setError($err);
        }

        // Finns det några meddelanden på någon vy?
        $message = parent::setViewMessages($installView); 


        $navHtml = $page->getNavigation();
        $loginHtml = $page->getLogin("");
        $headerHtml = $page->getHeader("Install");
        $bodyHtml = $page->getPage($body, $message);
        $infoHtml = $page->getInfoBar("");

        $site = $headerHtml . $loginHtml . $navHtml . $infoHtml . $bodyHtml;

        return $site;
	}
}

echo MasterInstaller::doInstall();