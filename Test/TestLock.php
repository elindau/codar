<?php
namespace test;

class TestLock {

	private $m_db = null;
	
	public function __construct($db) {
		$this->m_db = $db;
	}
	
	// Test av Lock-klassen
	public function TestLockHandler(){

		$codarDal = new \DBAccess\CodarDAL($this->m_db);
		$codar = new \model\Codar(0, "testCodar", 0, 0, 1, 0, 0, 2000, 5);
		$lock = new \model\Lock($this->m_db);

		// Insert Codarsen
		$codarId = $codarDal->insertCodar($codar);
		$codar->setCodarId($codarId);

		// Lås codar
		$codar->setCodarTrainingId(1);
		$codar->setTimeWhenLocked(microtime(true));
		$lock->lockCodar($codar);

		if($lock->isLocked($codarId) == false) {
			return false;
		}

		// Kolla så energin minskat med en
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_ID);
		$equals = array($codarId);
		$types = array(\DBAccess\CodarDAL::COL_CODAR_ID_TYPE);
		$codar = $codarDal->selectCodarBy($arguments, $equals, $types);

		if($codar->getEnergy() != 4) {
			return false;
		}

		// Ta bort från databasen
		if(!($codarDal->deleteCodar($codarId))) {
			return false;
		}

		return true;
	}
}