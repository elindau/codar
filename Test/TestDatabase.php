<?php
namespace test;

/**
 * Test av DAL-klasser som i stort sätt ser likadana ut
 * 
 * @package test
 */
class TestDatabase {

	private $m_db = null;
	private $m_username = "test";
	private $m_username2 = "testigen";

	public function __construct($db) {
		$this->m_db = $db;
	}

	/**
	 * Test av UserDAL-klassen
	 * @return boolean
	 */
	public function testUserDAL() {
		$userDAL = new \DBAccess\UserDAL($this->m_db);
		$user = new \model\User(0, $this->m_username, "test", "test@test.se");
		$user->setToken("testtoken");
		$user->setActive("emailActiveTest");

		// Insert
		if(!($userDAL->insertUser($user))) {
			return false;
		}

		// Hämta ut
		$DbUser = $userDAL->selectUserBy(array(\DBAccess\UserDAL::COL_USER_NAME),
												array($this->m_username), 
												array(\DBAccess\UserDAL::COL_USER_NAME_TYPE));

		if($DbUser == false) {
			return false;
		} else {
			if($DbUser->getUsername() != $user->getUsername()) {
				return false;
			}

			// Ska inte vara aktiv
			if($DbUser->getActive() == \model\User::ACTIVE) {
				return false;
			}

			// Ska inte ha en codar
			if($DbUser->getCodarId() != 0) {
				return false;
			}
		}

		// Uppdatera
		$arguments = array(\DBAccess\UserDAL::COL_USER_NAME);
		$equals = array($this->m_username2);
		$types = array(\DBAccess\UserDAL::COL_USER_NAME_TYPE);
		$outcome = $userDAL->updateUser($DbUser, $arguments, $equals, $types);

		if($outcome != true) {
			return false;
		}

		// Ta bort
		if(!($userDAL->deleteUser($DbUser->getUserId()))) {
			return false;
		}

		return true;
	}

	public function testCodarDAL() {
		$codarDal = new \DBAccess\CodarDAL($this->m_db);
		$codar = new \model\Codar(0, "testCodar", 0, 0, 1, 0, 0, 2000, 5);

		// Insert
		$codarId = $codarDal->insertCodar($codar);
		if(!($codarId)) {
			return false;
		}

		// Hämta ut på idt
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_ID);
		$equals = array($codarId);
		$types = array(\DBAccess\CodarDAL::COL_CODAR_ID_TYPE);
		$codar = $codarDal->selectCodarBy($arguments, $equals, $types);

		if($codar == false) {
			return false;
		}

		// Ta bort codarn
		if(!($codarDal->deleteCodar($codarId))) {
			return false;
		}

		return true;
	}
}