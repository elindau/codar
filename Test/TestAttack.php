<?php
namespace test;

class TestAttack {

	private $m_db = null;
	private $m_lostMoney = 0;
	private $m_earnedMoney = 0;
	private $m_earnedXp = 0;

	public function __construct($db) {
		$this->m_db = $db;
	}

	public function testAttackHandler() {
		$attackHandler = new \model\AttackHandler($this->m_db);
		$userDal = new \DBAccess\UserDAL($this->m_db);
		$codarDal = new \DBAccess\CodarDAL($this->m_db);
		$codar = new \model\Codar(0, "testCodar", 0, 0, 1, 0, 0, 2000, 5);
		$enemyCodar = new \model\Codar(0, "enemyCodar", 0, 0, 1, 0, 0, 2000, 5);

		// Insert Codarsen
		$codarId = $codarDal->insertCodar($codar);
		$eCodarId = $codarDal->insertCodar($enemyCodar);
		if(!($codarId) || !($eCodarId)) {
			return false;
		}

		$user = new \model\User(0, "testUser", "test", "test@test.se");
		$user->setToken("testtoken");
		$user->setActive("emailActiveTest");

		// Insert Usern
		if(!($userId = $userDal->insertUser($user))) {
			return false;
		}

		// Uppdatera användaren med codarIdt
		$user->setCodarId($codarId);
		$arguments = array(\DBAccess\UserDAL::COL_USER_CODAR_ID);
		$equals = array($codarId);
		$types = array(\DBAccess\UserDAL::COL_USER_CODAR_ID_TYPE);
		$outcome = $userDal->updateUser($user, $arguments, $equals, $types);

		if($outcome != true) {
			return false;
		}

		// 'Logga in' användaren
		\model\LoginHandler::updateLoggedInUser($user);

		// Attackera den andra codarn
		$outcome = $attackHandler->attackCodar($eCodarId);

		$expOutcomes = array(1, 2);
		$hit = 0;
		foreach($expOutcomes as $expected) {
			if($expected == $outcome) {
				$hit++;
			}
		}

		if($hit != 1) {
			return false;
		}

		// Spara undan resultat av match
		$this->m_earnedXp = $attackHandler->getEarnedExp();
		$this->m_earnedMoney = $attackHandler->getEarnedMoney(); 
		$this->m_lostMoney = $attackHandler->getLostMoney();

		// Ta bort från databasen
		if(!($codarDal->deleteCodar($codarId))) {
			return false;
		}

		if(!($codarDal->deleteCodar($eCodarId))) {
			return false;
		}

		if(!($userDal->deleteUser($userId))) {
			return false;
		}

		return true;

	}

	// Skriv ut
	public function getMatchResult() {
		return "<h3>Matchresultat</h3>
				<p>Förlorade pengar: $this->m_lostMoney</p>
				<p>Tjänade pengar: $this->m_earnedMoney</p>
				<p>Tjänad XP: $this->m_earnedXp</p>";
	}

}