<?php
namespace test;

class TestLogin {

	private $m_db = null;
	
	public function __construct($db) {
		$this->m_db = $db;
	}
	
	// Test av Login
	public function TestLogin(){
		$loginHandler = new \Model\LoginHandler($this->m_db);
		// State = utloggad
		$loginHandler->doLogout();

		// Testa om vi är inloggad, vid state = utloggad
		if($loginHandler->isLoggedIn()){
			echo "<p>Fel på funktionen \"isLoggedIn\"</p>";
			return false;
		}

		// Anrop av doLogin med felaktiga uppgifter
		if($loginHandler->doLogin("wrongusername", "wrongpassword")){
			echo "<p>Fel på funktionen \"doLogin\" vid fel användarnamn och lösenord</p>";
			return false;
		}

		// Anrop av doLogin med rätt uppgifter
		if(!($loginHandler->doLogin("admin", "k4Lm4r2012"))){
			echo "<p>Fel på funktionen \"doLogin\" vid rätt användarnamn och lösenord</p>";
			return false;
		}

		// Nu ska isLoggedIn retunera true, eftersom vi är inloggade
		if(!($loginHandler->isLoggedIn())){
			echo "<p>Fel på funktionen \"isLoggedIn\" vid state = inloggad</p>";
			return false;
		}

		// Logga ut
		$loginHandler->doLogout();

		// Testa doLogin med rätt användarnamn och fel lösenord
		if($loginHandler->doLogin("admin", "wrongpassword")){
			echo "<p>Fel på funktionen \"doLogin\" vid rätt användarnamn och fel lösenord</p>";
			return false;
		}

		// Allt fungerar som det ska
		return true;
	}
}