<?php
namespace test;

class TestLevel {

	private $m_db = null;
	private $m_add = 10;
	
	public function __construct($db) {
		$this->m_db = $db;
	}
	
	// Test av Level-klassen
	public function testLevelHandler(){
		$codarDal = new \DBAccess\CodarDAL($this->m_db);
		$codar = new \model\Codar(0, "testCodar", 0, 0, 1, 0, 0, 2000, 5);

		// Insert
		$codarId = $codarDal->insertCodar($codar);
		if(!($codarId)) {
			return false;
		}

		// Lägg till idt till objektet
		$codar->setCodarId($codarId);

		$level = new \model\Level($codar, $this->m_db);

		// Lägg till exp och pengar
		$level->addExpToCodar($this->m_add);
		$level->addMoneyToCodar($this->m_add + 2000);

		// Hämta ut codar igen
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_ID);
		$equals = array($codarId);
		$types = array(\DBAccess\CodarDAL::COL_CODAR_ID_TYPE);
		$codar = $codarDal->selectCodarBy($arguments, $equals, $types);

		// Fungerar det
		if($codar->getMoney() != $this->m_add + 2000 || $codar->getCodarExp() != $this->m_add) {
			return false;
		}

		// Ta bort pengar
		$before = $codar->getMoney();
		$level->removeMoneyFromCodar($codar->getMoney() - $this->m_add);

		// Hämta ut codar igen
		$codar = $codarDal->selectCodarBy($arguments, $equals, $types);

		// Test om det stämmer
		if($codar->getMoney() != $before - $this->m_add) {
			return false;
		}

		// Testa ta bort för mycket pengar
		if($level->removeMoneyFromCodar(3000)) {
			return false;
		}

		// Testa gå upp i level
		$level->addExpToCodar(51);

		// Hämta ut codar
		$codar = $codarDal->selectCodarBy($arguments, $equals, $types);

		if($codar->getCodarLevel() != 1) {
			return false;
		}

		// Ta bort codarn
		$codarDal->deleteCodar($codarId);

		return true;
	}

	public function testGameLostHandler() {
		// Skapa
		$codarDal = new \DBAccess\CodarDAL($this->m_db);
		$codar = new \model\Codar(0, "testCodar", 1, 0, 1, 0, 0, 2000, 5);
		$level = new \model\Level($codar, $this->m_db);

		// Insert
		$codarId = $codarDal->insertCodar($codar);
		if(!($codarId)) {
			return false;
		}

		// Ta bort en level
		$codar->setCodarId($codarId);
		$level->deLevelCodar($codar);

		// Hämta ut codar igen
		$arguments = array(\DBAccess\CodarDAL::COL_CODAR_ID);
		$equals = array($codarId);
		$types = array(\DBAccess\CodarDAL::COL_CODAR_ID_TYPE);
		$codar = $codarDal->selectCodarBy($arguments, $equals, $types);

		if($codar->getMoney() > 2000 || $codar->getCodarLevel() != 0) {
			return false;
		}

		/** OBS! Test av GameLost-funktion **/
		/*$userDal = new \DBAccess\UserDAL($this->m_db);

		$user = new \model\User(0, "testUser", "test", "test@test.se");
		$user->setToken("testtoken");
		$user->setActive("emailActiveTest");

		// Insert Usern
		if(!($userId = $userDal->insertUser($user))) {
			return false;
		}

		// Uppdatera usern
		$user->setUserId($userId);
		$arguments = array(\DBAccess\UserDAL::COL_USER_CODAR_ID);
		$equals = array($codarId);
		$types = array(\DBAccess\UserDAL::COL_USER_CODAR_ID_TYPE);
		$userDal->updateUser($user, $arguments, $equals, $types);

		$level->deLevelCodar($codar);*/

		// Ta bort codarn
		$codarDal->deleteCodar($codarId);

		return true;
	}
}