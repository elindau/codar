<?php
namespace view;

class RegisterUser extends MasterView {

	const m_regNewUser = "regnewuser";

	private $m_regUserKey = "reguser";
	private $m_regPassKey = "regpass";
	private $m_regPassRepeatKey = "regpassrep";
	private $m_regEmailKey = "regemail";
	private $m_regSubKey = "regsubmit";

	public function getUsername() {
		if(isset($_POST[$this->m_regUserKey])) {
			return $_POST[$this->m_regUserKey];
		}
		return null;
	}

	public function getPassword() {
		if(isset($_POST[$this->m_regPassKey])) {
			return $_POST[$this->m_regPassKey];
		}
		return null;
	}

	public function getPasswordRepeat() {
		if(isset($_POST[$this->m_regPassRepeatKey])) {
			return $_POST[$this->m_regPassRepeatKey];
		}
		return null;
	}

	public function getEmail() {
		if(isset($_POST[$this->m_regEmailKey])) {
			return $_POST[$this->m_regEmailKey];
		}
		return null;
	}

	public function triedToRegister() {
		return isset($_POST[$this->m_regSubKey]);
	}

	public function triedToNewRegister() {
		return isset($_GET[self::m_regNewUser]);
	}

	public static function doNewRegisterButton() {
		return "<a href='" . \view\Navigation::generateRegisterGet() . "'>Registrera ny användare</a>";
	}

	public function doBackButton() {
		return "<a href='" . \view\Navigation::generateMainGet() . "'>Tillbaka</a>";
	}

	public function doRegisterForm() {
		return "
			<h2>Registrera ny användare</h2>
			<p>Observera att mailen måste vara giltlig då det skickas ett aktiveringsmail dit innan du kan logga in</p>
			<div id='register'>
			<form action='". \view\Navigation::generateRegisterGet() ."' method='post'>
				<input type='text' id='name' name='$this->m_regUserKey' placeholder='Användarnamn' required autofocus />
				<input type='password' id='pw' name='$this->m_regPassKey' placeholder='Lösenord' required />
				<input type='password' id='pwRepeat' name='$this->m_regPassRepeatKey' placeholder='Repetera lösenord' required />
				<input type='email' id='email' name='$this->m_regEmailKey' placeholder='Email' required />
				<input type='submit' name='$this->m_regSubKey' value='Registrera' />
			</form>
			</div>";
	}
}