<?php
namespace view;

class AdminView extends MasterView {
	
	private $m_dropDownKey = "dropDownUsers";
	private $m_delSubKey = "deleteUser";

	// Ändra användare
	private $m_editSubKey = "editUser";
	private $m_editUserKey = "editUserKey";
	private $m_editPassKey = "editPassKey";
	private $m_editPassRepeatKey = "editPassRepeatKey";
	private $m_editEmailKey = "editEmailKey";
	private $m_doEditSubKey = "doEditSubKey";
	private $m_editUserId = "editUserId";
	private $m_editUserOldPw = "editUserOldPw";
	private $m_editActiveKey = "editActiveKey";

	// Lägg till användare
	private $m_addSubKey = "addSubKey";
	private $m_addUserKey = "addUserKey";
	private $m_addPassKey = "addPassKey";
	private $m_addPassRepeatKey = "addPassRepeatKey";
	private $m_addEmailKey = "addEmailKey";
	private $m_doAddSubKey = "doAddSubKey";

	// Retunerar en populerad drop-down med alla användare i databasen
	public function doDeleteUserForm($users) {

		$options = "";
		foreach($users as $user) {
			$id = $user->getUserId();
			$name = $user->getUsername();

			$options .= "<option value='$id'>$name</option>";
		}

		return "
		<h2>Administratör-inställningar</h2>
		<div id='admin'>
		<form action='' method='post'>
					<select name='$this->m_dropDownKey'>
						$options
					</select>
					<input type='submit' name='$this->m_delSubKey' value='Ta bort användare' />
					<input type='submit' name='$this->m_editSubKey' value='Ändra användare' />
					<input type='submit' name='$this->m_addSubKey' value='Lägg till användare' />
				</form></div>";
	}

	public function doEditForm(\model\User $user) {

		$name = $user->getUsername();
		$pw = $user->getPassword();
		$email = $user->getEmail();
		$id = $user->getUserId();
		$actives = array(\model\User::ACTIVE, \model\User::INACTIVE);

		$options = "";
		foreach($actives as $active) {
			if($active == \model\User::ACTIVE) {
				$aname = "Aktiv";
			} else {
				$aname = "Inaktiv";
			}

			if($user->getActive() == $active) {
				$options .= "<option value='$active' selected>$aname</option>";
			} else {
				$options .= "<option value='$active'>$aname</option>";
			}
		}

		return "
		<h2>Ändra användare</h2>
		<div id='register'>
			<form action='' method='post'>
				<input type='hidden' name='$this->m_editUserId' value='$id' />
				<input type='hidden' name='$this->m_editUserOldPw' value='$pw' />
				<input type='text' id='name' name='$this->m_editUserKey' value='$name' required />
				<input type='password' id='pw' name='$this->m_editPassKey' value='$pw' required />
				<input type='password' id='pwRepeat' name='$this->m_editPassRepeatKey' value='$pw' required />
				<input type='email' id='email' name='$this->m_editEmailKey' value='$email' required />
				<select name='$this->m_editActiveKey'>
					$options
				</selec>
				<input type='submit' name='$this->m_doEditSubKey' value='Spara' />
			</form></div>";
	}

	public function doAddForm() {
		return "
		<h2>Lägg till användare</h2>
		<div id='register'>
			<form action='' method='post'>
				<input type='text' id='name' name='$this->m_addUserKey' placeholder='Användarnamn' required autofocus />
				<input type='password' id='pw' name='$this->m_addPassKey' placeholder='Lösenord' required />
				<input type='password' id='pwRepeat' name='$this->m_addPassRepeatKey' placeholder='Repetera lösenord' required />
				<input type='email' id='email' name='$this->m_addEmailKey' placeholder='Email' required />
				<input type='submit' name='$this->m_doAddSubKey' value='Lägg till' />
			</form></div>";
	}

	// Tryck lägg till
	public function didDoAdd() {
		return isset($_POST[$this->m_doAddSubKey]);
	}

	// Tryck lägg till användare
	public function didAdd() {
		return isset($_POST[$this->m_addSubKey]);
	}

	public function didDelete() {
		return isset($_POST[$this->m_delSubKey]);
	}

	public function didEdit() {
		return isset($_POST[$this->m_editSubKey]);
	}

	public function didSaveEdit() {
		return isset($_POST[$this->m_doEditSubKey]);
	}

	public function getUserId() {
		if(isset($_POST[$this->m_dropDownKey])) {
			return $_POST[$this->m_dropDownKey];
		} else if(isset($_POST[$this->m_editUserId])) {
			return $_POST[$this->m_editUserId];
		}
		return null;
	}

	// Användarens nya uppgifter
	public function getUsername() {
		if(isset($_POST[$this->m_editUserKey])) {
			return $_POST[$this->m_editUserKey];
		} else if(isset($_POST[$this->m_addUserKey])) {
			return $_POST[$this->m_addUserKey];
		} else {
			return null;
		}
	}

	public function getPassword() {
		if(isset($_POST[$this->m_editPassKey])) {
			return $_POST[$this->m_editPassKey];
		} else if(isset($_POST[$this->m_addPassKey])) {
			return $_POST[$this->m_addPassKey];
		} else {
			return null;
		}
	}

	public function getPasswordRepeat() {
		if(isset($_POST[$this->m_editPassRepeatKey])) {
			return $_POST[$this->m_editPassRepeatKey];
		} else if(isset($_POST[$this->m_addPassRepeatKey])) {
			return $_POST[$this->m_addPassRepeatKey];
		} else {
			return null;
		}
	}

	public function getEmail() {
		if(isset($_POST[$this->m_editEmailKey])) {
			return $_POST[$this->m_editEmailKey];
		} else if(isset($_POST[$this->m_addEmailKey])) {
			return $_POST[$this->m_addEmailKey];
		} else {
			return null;
		}
	}

	public function getActive() {
		return isset($_POST[$this->m_editActiveKey]) == true ? $_POST[$this->m_editActiveKey] : null;
	}

	public function getOldPassword() {
		return isset($_POST[$this->m_editUserOldPw]) == true ? $_POST[$this->m_editUserOldPw] : null;
	}

}