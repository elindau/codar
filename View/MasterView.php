<?php
namespace view;

class MasterView {

	private $m_errorMessages = array();
	private $m_successMessages = array();

	public function setError($error) {
		$this->m_errorMessages[] = $error;
	}

	public function setSuccess($message) {
		$this->m_successMessages[] = $message;
	}

	// Finns det felmeddelanden
	public function isValid() {
		if(count($this->m_errorMessages) != 0) {
			return false;
		}
		return true;
	}

	// Finns det andra meddelanden
	public function isMessage() {
		if(count($this->m_successMessages) == 0) {
			return false;
		}
		return true;
	}

	// Visa felmeddelanden
	public function doErrors() {
		$errorDiv = "<div id='errors'>";
		foreach($this->m_errorMessages as $value) {
			$errorDiv .= "<div class='error'> $value </div>";
		}
		$errorDiv .= "</div>";

		return $errorDiv;
	}

	// Visa rättmeddelande
	public function doSuccess($message) {
		return "
			<div id='success'>
				$message
			</div>
			";
	}

	public function doMessages() {
		$successDiv = "<div id='success'>";
		foreach($this->m_successMessages as $value) {
			$successDiv .= "<div class='success'> $value </div>";
		}
		$successDiv .= "</div>";

		return $successDiv;
	}

}