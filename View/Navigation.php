<?php
namespace view;

class Navigation {

	const Controller = "Controller";

	const UserViewsMain = "Main";
	const UserViewsRegisterForm = "RegisterForm";
	const UserValidatesEmail = "ActivateMail";
	const UserForgotPw = "ForgotPw";
	const UserId = "userId";
	const UserGeneratesCodar = "CodarForm";
	const UserViewsCodar = "Codar";
	const CodarId = "CodarId";
	const UserViewsAttacks = "Attacks";
	const UserViewsTraining = "Training";
	const UserViewsShop = "Shop";
	const AdminView = "Admin";
	const UserDeletesMessage = "Delete";
	const MessageId = "MessageId";

	public function getActiveType() {
		if(isset($_GET[Navigation::Controller])) {
			return $_GET[Navigation::Controller];
		}
		return null;
	}


	public static function generateRegisterGet() {
		return "?" . self::Controller . "=" . self::UserViewsRegisterForm;
	}

	public static function generateMainGet() {
		return "?" . self::Controller . "=" . self::UserViewsMain;
	}

	public static function generateMailGet() {
		return "?" . self::Controller . "=" . self::UserValidatesEmail . "&" . self::UserId . "=";
	}

	public static function generatePwForget() {
		return "?" . self::Controller . "=" . self::UserForgotPw;
	}

	public static function generateCodarFormGet() {
		return "?" . self::Controller . "=" . self::UserGeneratesCodar;
	}

	public static function generateCodarGet() {
		return "?" . self::Controller . "=" . self::UserViewsCodar . "&" . self::CodarId . "=";
	}

	public static function generateAttackGet() {
		return "?" . self::Controller . "=" . self::UserViewsAttacks;
	}

	public static function generateTrainingGet() {
		return "?" . self::Controller . "=" . self::UserViewsTraining;
	}

	public static function generateShopGet() {
		return "?" . self::Controller . "=" . self::UserViewsShop;
	}

	public static function generateAdminGet() {
		return "?" . self::Controller . "=" . self::AdminView;
	}

	public static function generateDeleteMessageGet() {
		return "?" . self::Controller . "=" . self::UserDeletesMessage . "&" . self::MessageId . "=";
	}

}