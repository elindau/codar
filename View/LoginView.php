<?php
namespace view;

class LoginView extends MasterView {

	// Medlemsvariabler för att undvika strängberoenden
	private $m_userKey = "username";
	private $m_pwKey = "password";
	private $m_subKey = "submit";
	private $m_logKey = "logout";
	private $m_rememberKey = "remember";
	private $m_cookieUserKey = "cookieUser";
	private $m_cookiePwKey = "cookiePw";

	private $m_cookieUser = "";
	private $m_cookiePw = "";

	// Hämta användarnamn från POST-arrayen om det finns
	public function getUsername() {
		if(isset($_POST[$this->m_userKey])){
			return $_POST[$this->m_userKey];
		}
		return null;
	}

	// Hämta ut token från COOKIE om det finns
	public function getTokenFromCookie() {
		if(isset($_COOKIE[$this->m_cookieUserKey])) {
			return $_COOKIE[$this->m_cookieUserKey];
		}
	}

	// Hämta lösenord från POST-arrayen om det finns
	public function getPassword() {
		if(isset($_POST[$this->m_pwKey])){
			return $_POST[$this->m_pwKey];
		}
		return null;
	}

	// Har användaren tryckt på login
	public function triedToLogin() {
		return isset($_POST[$this->m_subKey]);
	}

	// Har användaren tryckt på logout
	public function triedToLogout() {
		return isset($_POST[$this->m_logKey]);
	}

	// Har användaren fyllt i Remember me
	public function checkedRememberMe() {
		return isset($_POST[$this->m_rememberKey]);
	}

	// Spara undan cookie
	/*
	public function DoRememberMe($user, $pw) {
		setcookie($this->m_cookieUserKey, $user, time() + 1800);
		$_COOKIE[$this->m_cookieUserKey] = $user;

		setcookie($this->m_cookiePwKey, $pw, time() + 1800);
		$_COOKIE[$this->m_cookiePwKey] = $pw;
	}
	*/

	public function doRememberMe($token) {
		setcookie($this->m_cookieUserKey, $token, time() + 1800);
		$_COOKIE[$this->m_cookieUserKey] = $token;
	}

	// Ta bort cookie
	public function dropCookie() {
		unset($_COOKIE[$this->m_cookieUserKey]);

		// Lurar webbläsaren att cookien expirat
		setcookie($this->m_cookieUserKey, "", time() - 1800);
	}

	// Retunerar ett login-forumlär
	public function doLoginBox() {
		$newRegButton = \view\RegisterUser::doNewRegisterButton();
		return "
		<form action=\"index.php\" method='post'>
			<input type='text' id='name' name='$this->m_userKey' placeholder='Användarnamn' value='$this->m_cookieUser' required autofocus />
				<input type='password' id='pw' name='$this->m_pwKey' placeholder='Lösenord' value='$this->m_cookiePw' required />
				<input type='submit' name='$this->m_subKey' value='Login' />
	
				<label for='remember'>Kom ihåg</label>
				<input type='checkbox' name='$this->m_rememberKey' id='remember' />

		</form>
		<div id='reg'>
		<a href='" . \view\Navigation::generatePwForget() . "'>Glömt lösenord?</a>
		$newRegButton
		</div>";
	}

	// Retunerar en logout-knapp
	public function doLogoutBox($name) {
		return "
		<p> Du är inloggad som $name </p>
		<form action=\"index.php\" method='post'>
					<input type='submit' name='$this->m_logKey' value='Logout' />
				</form>";
	}
}
