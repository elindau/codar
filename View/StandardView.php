<?php
namespace view;

class StandardView extends MasterView {

	public function doMainPage() {
		return "<h2>Välkommen till Codar!</h2>
				<p>Är du redo att bli den bästa Codarn?
				 Är du redo att lägga ifrån dig ditt vanliga liv och dedikera timmar till att bli oövervinnlig? 
				 I sådana fall, registrera dig.</p>";
	}
}