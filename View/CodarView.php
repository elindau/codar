<?php

namespace view;

class CodarView extends MasterView {

	private $m_nameKey = "name";
	private $m_languageKey = "language";
	private $m_submitKey = "submitCodar";

	public function doCodarForm($languages) {

		$options = "";
		foreach($languages as $id => $lang) {
			$options .= "<option value='$id'>$lang</option>";
		}

		return "
		<h2>Skapa Codar</h2>
		<p>Välkommen! Du måste nu skapa din Codar, välj ett namn och ett språk som din Codar ska använda sig av</p>
			<form action='index.php' method='post'>
				<input type='text' id='name' name='$this->m_nameKey' placeholder='Namn' required autofocus />
				<select name='$this->m_languageKey'>
					$options
				</select>
				<input type='submit' name='$this->m_submitKey' value='Acceptera' />
			</form>";
	}

	public function getLanguageId() {
		return isset($_POST[$this->m_languageKey]) == true ? $_POST[$this->m_languageKey] : null;
	}

	public function getCodarName() {
		return isset($_POST[$this->m_nameKey]) == true ? $_POST[$this->m_nameKey] : null;
	}

	public function didSubmit() {
		return isset($_POST[$this->m_submitKey]);
	}

	public function setError($error) {

		switch($error) {
			case \model\CodarHandler::ERROR_FAILED_TO_INSERT:
				parent::setError(\Common\Strings::e_failedInsert);
			break;

			case \model\CodarHandler::ERROR_VALIDATION:
				parent::setError(\Common\Strings::e_nameFormat);
			break;
		}
	}
}