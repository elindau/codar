<?php

namespace view;

class MainView extends MasterView {

	public function presentCodar(\model\Codar $codar, $time) {
		$name = $codar->getCodarName();
		$lvl = $codar->getCodarLevel();
		$imgLvl = $codar->getCodarImageName();
		$exp = $codar->getCodarExp();
		$money = $codar->getMoney();
		$sign = \model\Money::SIGN;

		$lockedHtml = "";
		if($codar->getTimeWhenLocked() != 0) {
			if($time <= 0) {
				$time = 0;
			}
			$lockedHtml = "<p>Lås kvar: $time min";
		}


		//$level = new \model\Level($codar);
		$maxExp = \model\Level::getCodarMaxExp($codar);
		$energy = $codar->getEnergy();

		return "<div id='codar'>
				<h2>$name</h2>
				<img src='Common/img/codar/$imgLvl.png' />
					<p>Level: $lvl</p>
					<p>$exp / $maxExp XP</p>
					<p>$money $sign</p>
					$lockedHtml
					<p><img src='Common/img/energy/".$energy.".png' /></p>
				</div>";
	}

	public function doToplist($codars) {
		$list = "<ul>";
		$i = 1;
		foreach($codars as $codar) {
			$list .= "<li>
						<a href='".\view\Navigation::generateCodarGet().$codar->getCodarId()."'>
						<p>$i. " . $codar->getCodarName() . "</p>
						<p>Level: " . $codar->getCodarLevel() . "</p>
						</a>
					</li>";
			$i++;
		}
		$list .= "</ul>";
		$div = "<div id='topList'>
					<h2>Topplistan</h2>
					$list
				</div>";
		return $div;
	}

	// Hämta ut codarIdt från GET
	public function getCodarId() {

		if(isset($_GET[\view\Navigation::CodarId])) {
			return $_GET[\view\Navigation::CodarId];
		}
		return null;
	}

	// Överlagra basklassens setError
	public function setError($error) {

		switch($error) {
			case \model\MainHandler::ERROR_NO_CODAR:
				parent::setError(\Common\Strings::e_noCodar);
			break;

			default:
				parent::setError($error);
			break;
		}
	}
}