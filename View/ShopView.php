<?php

namespace view;

class ShopView extends MasterView {

	const MAX_FILE_SIZE = "10000";

	private $m_itemPost = "itemPost";
	private $m_buyPost = "buyShop";
	private $m_adminAddItem = "adminAddItem";
	private $m_addItem = "addItem";
	private $m_dropDownValue = "dropDownValue";
	private $m_addItemNameKey = "addItemNameKey";
	private $m_addItemCostKey = "addItemCostKey";
	private $m_addfileKey = "fileKey";
	private $m_addFileSize = "addFileSize";
	private $m_deleteItemPost = "deleteItemPost";

	// Visa alla items från databasen
	public function doShopList($items, $admin) {

		$button = "";
		$delete = "";
		if($admin) {
			$button = $this->doAdminAddItemButton();
			$delete = "<button type='submit' name='$this->m_deleteItemPost' >Ta bort</button>";
		}

		$list = "<h2>Energishop</h2><div id='items'><ul>";

		if(count($items) == 0) {
			$list .= "<p>Finns inget i shoppen!</p>";
		}

		foreach($items as $item) {
			$src = $item->getFileSource();
			$list .= "
			<div class='shopList'>
			<img src='$src' />
				<li>
						
						<p>" . $item->getName() ."</p>
						<p>" . $item->getDescr() ."</p>
						<p>" . $item->getCost() . " " . \model\Money::SIGN . "</p>
						<form action='' method='post'>
							<input type='hidden' name='$this->m_itemPost' value='". $item->getId() ."' />
							<button type='submit' name='$this->m_buyPost' >Köp</button>
							$delete
						</form>
					
				</li></div>";
		}
		$list .= "</ul></div>$button";
		return $list;
	}

	private function doAdminAddItemButton() {
		return "<form action='' method='post'>
					<button type='submit' name='$this->m_adminAddItem' >Lägg till item</button>
				</form>";
	}

	public function doAddItemForm() {
		$options = "";
		for($i = \model\Item::MIN_VALUE; $i <= \model\Item::MAX_VALUE; $i++) {
			$options .= "<option value='$i'>$i</option>";
		}
		$size = self::MAX_FILE_SIZE;
		return "<form enctype='multipart/form-data' action='' method='post'>
					<input type='text' id='name' name='$this->m_addItemNameKey' placeholder='Namn' required autofocus />
					<input type='text' id='cost' name='$this->m_addItemCostKey' placeholder='Kostnad' required autofocus />
					<select name='$this->m_dropDownValue'>
						$options
					</select>
					<input type='hidden' name='$this->m_addFileSize' value='$size' />
					<input name='$this->m_addfileKey' type='file' />
					<button type='submit' name='$this->m_addItem' >Lägg till</button>
				</form>";
	}

	public function didAdd() {
		return isset($_POST[$this->m_addItem]) == true ? true : false;
	}

	public function didAdminAdd() {
		return isset($_POST[$this->m_adminAddItem]) == true ? true : false;
	}

	// Har köp tryckts
	public function didBuy() {
		return isset($_POST[$this->m_buyPost]) == true ? true : false;
	}

	// Har delete tryckts
	public function didDelete() {
		return isset($_POST[$this->m_deleteItemPost]) == true ? true : false;
	}

	// Hämta ut itemet
	public function getItemToBuy() {
		return isset($_POST[$this->m_itemPost]) == true ? $_POST[$this->m_itemPost] : null;
	}

	// Hämta ut filen
	public function getFile() {
		return $_FILES[$this->m_addfileKey];
	}

	// Hämta ut namn
	public function getItemName() {
		return isset($_POST[$this->m_addItemNameKey]) == true ? $_POST[$this->m_addItemNameKey] : null;
	}

	// Hämta ut kostnad
	public function getItemCost() {
		return isset($_POST[$this->m_addItemCostKey]) == true ? $_POST[$this->m_addItemCostKey] : null;
	}

	// Hämta ut värde
	public function getItemValue() {
		return isset($_POST[$this->m_dropDownValue]) == true ? $_POST[$this->m_dropDownValue] : null;
	}

	// Överlagra basklassens setError
	public function setError($error) {

		switch($error) {

			case \model\ShopHandler::ERROR_CANT_AFFORD:
				parent::setError("Du har inte råd!");
			break;

			case \model\ShopHandler::ERROR_FULL_ENERGY:
				parent::setError("Du har redan full energi!");
			break;

			case UPLOAD_ERR_INI_SIZE:
				parent::setError(\Common\Strings::e_maxFileSize2);
			break;

			case UPLOAD_ERR_FORM_SIZE:
				parent::setError(\Common\Strings::e_maxFileSize);
			break;

			case UPLOAD_ERR_PARTIAL:
				parent::setError(\Common\Strings::e_partialUpload);
			break;

			case UPLOAD_ERR_NO_FILE:
				parent::setError(\Common\Strings::e_noFile);
			break;

			case UPLOAD_ERR_NO_TMP_DIR:
				// Tempfoldern hittades inte
				parent::setError(\Common\Strings::e_noTmpFolder);
			break;

			case UPLOAD_ERR_CANT_WRITE:
				parent::setError(\Common\Strings::e_saving);
			break;

			case UPLOAD_ERR_EXTENSION:
				parent::setError(\Common\Strings::illegalExtension);
			break;

			case \model\ShopHandler::ERROR_HTTP:
				parent::setError("Fel!");
			break;

			case \model\ShopHandler::ERROR_FILE_SIZE:
				parent::setError("Bilden är för stor!");
			break;

			case \model\ShopHandler::ERROR_EXT:
				parent::setError("Bilden måste vara av typ .png");
			break;

			case \model\ShopHandler::ERROR_TYPE:
				parent::setError("Filen måste vara en bild image/png");
			break;

			case \model\ShopHandler::ERROR_MOVE:
				parent::setError("Gick inte att spara filen");
			break;

			case \model\ShopHandler::ERROR_NAME:
				parent::setError("Fel i namnet");
			break;

			case \model\ShopHandler::ERROR_NUMBER:
				parent::setError("Fel kostnad");
			break;

			default:
				parent::setError($error);
			break;
		}
	}
}