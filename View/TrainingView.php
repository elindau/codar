<?php

namespace view;

class TrainingView extends MasterView {

	private $m_typeSubmit = "typeSubmit";
	private $m_selectType = "selectType";
	private $m_buyId = "buyId";
	private $m_buyPost = "buyPost";

	public function doTrainingForm($trainingTypes) {

		$options = "";
		foreach($trainingTypes as $types) {
			$options .= "<option value='" . $types->getTrainingId() . "' >" .$types->getName() . " : " . $types->getValue() . " XP : " . $types->getTimelock() . " MIN</option>";
		}

		return "
		<div id='left'>
		<h2>Träning</h2>
		<form action='' method='post'>
			<select name='$this->m_selectType'>
				$options
			</select>
			<input type='submit' name='$this->m_typeSubmit' value='Skicka på träning' />
		</form>
		</div>";
		
	}

	public function doBuyForm($trainings) {
		$list = "<ul>";
		foreach($trainings as $training) {
			$list .= "<li>
						<p>" . $training->getName() . "</p>
						<p>" . $training->getValue() . " XP</p>
						<p>" . $training->getTimelock() ." MIN</p>
						<p>" . $training->getCost() . " " . \model\Money::SIGN ."</p>
							<form action='' method='post'>
								<input type='hidden' name='$this->m_buyId' value='". $training->getTrainingId() ."' />
								<button type='submit' name='$this->m_buyPost' >Köp ". $training->getName() ."</button>
							</form>
					</li>";
		}
		$list .= "</ul>";
		$div = "<div id='right'>
				<div id='shop'>
					<h2>Shop</h2>
					$list
				</div>
				</div>";
		return $div;
	}

	public function isTrainingPressed() {
		return isset($_POST[$this->m_typeSubmit]) == true ? true : false;
	}

	public function getTrainingType() {
		return isset($_POST[$this->m_typeSubmit]) == true ? $_POST[$this->m_selectType] : null;
	}

	public function isBuyPressed() {
		return isset($_POST[$this->m_buyPost]) == true ? true : false;
	}

	public function getBuyTrainingType() {
		return isset($_POST[$this->m_buyPost]) == true ? $_POST[$this->m_buyId] : null;
	}

	// Lägger till fel i basklassen
	public function setError($error) {

		switch($error) {
			case \model\TrainingHandler::ERROR_LOCKED:
				parent::setError(\Common\Strings::e_lockedCodar);
			break;

			case \model\TrainingHandler::ERROR_CANT_AFFORD:
				parent::setError("Du har inte råd!");
			break;

			case \model\TrainingHandler::ERROR_NO_ENERGY:
				parent::setError("Din Codar har ingen energi!");
			break;
		}
	}

}