<?php

namespace view;

class ValidMailView extends MasterView {

	// Hämta ut userID från GET (eg. mailIdt)
	public function getUserId() {

		if(isset($_GET[\view\Navigation::UserId])) {
			return $_GET[\view\Navigation::UserId];
		}
		return null;
	}

	// Länk tillbaka till inlogg
	public function doReturn() {
		return "<a href='"  . \view\Navigation::generateMainGet() . "'> Till inlogg </a>";
	}

	// Lägger till fel i basklassen
	public function setError($error) {

		switch($error) {
			case \model\ValidMailHandler::ERROR_ALREADY_ACTIVE:
				parent::setError(\Common\Strings::e_alreadyActive);
			break;

			case \model\ValidMailHandler::ERROR_WRONG_ID:
				parent::setError(\Common\Strings::e_wrongId);
			break;

			case \model\ValidMailHandler::ERROR_COULDNT_ACTIVATE:
				parent::setError(\Common\Strings::e_couldntActivate);
			break;
		}
	}
}