<?php

namespace view;

class AttackView  extends MasterView {

	private $m_attackPost = "attackPost";
	private $m_codarPost = "codarPost";

	/**
	 * Genererar en lista med alla Codars
	 * @param array $codars 
	 * @return string html
	 */
	public function doAllCodarsList($codars) {

		$list = "<h2>Attackera codars</h2><div id='attacks'><ul>";
		if(count($codars) == 0) {
			$list .= "<p>Finns inga codars att attackera!</p>";
		}
		foreach($codars as $codar) {
			$imgName = $codar->getCodarImageName();
			$list .= "
			<div class='codarList'>
			<img src='Common/img/codar/$imgName.png' />
				<li>
						
						<p>" . $codar->getCodarName() ."</p>
						<p>Level: " . $codar->getCodarLevel() . "</p>
						<form action='' method='post'>
							<input type='hidden' name='$this->m_codarPost' value='". $codar->getCodarId() ."' />
							<button type='submit' name='$this->m_attackPost' >Attackera ". $codar->getCodarName() ."</button>
						</form>
					
				</li></div>";
		}
		$list .= "</ul></div>";
		return $list;
	}

	public function didAttack() {
		return isset($_POST[$this->m_attackPost]) == true ? true : false;
	}

	public function getCodarToAttack() {
		return isset($_POST[$this->m_attackPost]) == true ? $_POST[$this->m_codarPost] : null;
	}
}