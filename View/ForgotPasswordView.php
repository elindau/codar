<?php

namespace view;

class ForgotPasswordView extends MasterView {

	private $m_subForgotKey = "subForgotKey";
	private $m_emailKey = "email";

	// Hämta ut mailen
	public function getEmail() {
		if(isset($_POST[$this->m_emailKey])) {
			return $_POST[$this->m_emailKey];
		}
		return null;
	}

	// Har användaren tryckt på knapp
	public function triedToSend() {
		return isset($_POST[$this->m_subForgotKey]);
	}

	public function doForgotPwMail() {
		return "
		<h2>Glömt lösenord</h2>
		<p>Glömt ditt lösenord? Inga problem, vi skickar ett nytt till dig!</p>
			<form action='". \view\Navigation::generatePwForget() ."' method='post'>
				<input type='email' id='email' name='$this->m_emailKey' placeholder='Email' required />
				<input type='submit' name='$this->m_subForgotKey' value='Skicka mail' />
			</form>";
	}

	public function doBackButton() {
		return "<a href='" . \view\Navigation::generateMainGet() . "'>Tillbaka</a>";
	}

	// Lägger till fel i basklassen
	public function setError($error) {

		switch($error) {
			case \model\ForgotPasswordHandler::ERROR_FAILED_TO_SEND:
				parent::setError(\Common\Strings::e_failedtoSendEmail);
			break;

			case \model\ForgotPasswordHandler::ERROR_NO_EMAIL:
				parent::setError(\Common\Strings::e_noEmail);
			break;

			case \model\ForgotPasswordHandler::ERROR_WRONG_EMAIL_FORMAT:
				parent::setError(\Common\Strings::e_emailFormat);
			break;
		}
	}
}