<?php

namespace view;

class MessageView extends MasterView {

	private $m_showMessageKey = "showMessageKey";
	private $m_deleteAll  = "deleteAll";

	public function doNoMessageButton() {
		return "
		<ul class='ddmessages'>
		<li><button id='nomessage' type='submit' onclick=\"javascript:showlayer('mm_2')\"></button></li>
			<ul class='messagedd' id='mm_2'>
			<li>Inga meddelanden!</li>
			</ul>
		</ul>
		";
	}

	public function doGotMessageButton($messages) {

		$lis = "";
		foreach($messages as $message) {
			$msg = $message->getMessage();
			$link = \view\Navigation::generateDeleteMessageGet().$message->getMessageId();
			$lis .= "<li><a href='$link'>$msg</a></li>";
		}

		$deleteAllLink = "";
		if(count($messages) > 1) {
			$deleteAllLink = "<li><a href='?$this->m_deleteAll'>Ta bort alla meddelande</a></li>";
		}

		return "
		<ul class='ddmessages'>
		<li><button id='gotmessage' type='submit' onclick=\"javascript:showlayer('mm_1')\"></button></li>
			<ul class='messagedd' id='mm_1'>
				$deleteAllLink
				$lis
			</ul>
		</ul>
		";
	}

	// Hämta ut messageIdt från GET
	public function getMessageId() {

		if(isset($_GET[\view\Navigation::MessageId])) {
			return $_GET[\view\Navigation::MessageId];
		}
		return null;
	}

	// Har meddelandet tryckts
	public function didDelete() {
		return isset($_GET[\view\Navigation::MessageId]);
	}

	public function didDeleteAll() {
		return isset($_GET[$this->m_deleteAll]);
	}
}