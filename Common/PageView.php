<?php

namespace Common;

class PageView {

	// Retunerar header-delen av sidan
	public function getHeader($title) {

		$style1 = Apperence::STYLE1;
		$style2 = Apperence::STYLE2;

		$style = Apperence::getStyle();

		$header = "<img src='Common/img/header.png' />";
		if($style == Apperence::STYLE2) {
			$header = "<img src='Common/img/header2.png' />";
		}
		

		return "<!doctype html>
			<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
			<!--[if lt IE 7]> <html class='no-js lt-ie9 lt-ie8 lt-ie7' lang='en'> <![endif]-->
			<!--[if IE 7]>    <html class='no-js lt-ie9 lt-ie8' lang='en'> <![endif]-->
			<!--[if IE 8]>    <html class='no-js lt-ie9' lang='en'> <![endif]-->
			<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
			<!--[if gt IE 8]><!--> <html class='no-js' lang='en'> <!--<![endif]-->
			<head>
  				<meta charset='utf-8'>

  				<title>$title</title>
  				<meta name='description' content=''>

  				<link rel='stylesheet' href='css/$style.css'>

			</head>
			<body>
				<div id='styleChange' >
				<form action='' method='post'>
					<button id='style1' type='submit' name='$style1'></button>
					<button id='style2' type='submit' name='$style2'></button>
				</form>
					<a href='#' class='style2'></a>
				</div>
 			<header>
 			$header
 			";
	}

	// Kollar vilken style.css som är aktiv
	public function isStyle1Set() {
		return isset($_POST[Apperence::STYLE1]);
	}

	public function isStyle2Set() {
		return isset($_POST[Apperence::STYLE2]);
	}

	// Retunerar login-delen av sidan
	public function getLogin($login) {
		return "<div id='login'>$login</div></header>";

	}

	// Retunerar navigations-delen av sidan
	public function getNavigation() {
		return "<div id='navigation'>
				<ul>
					<li><a href='".\view\Navigation::generateMainGet()."'>Hem</a></li>
				</ul>
				</div>";
	}

	// Retunera info-delen
	public function getInfoBar($info) {
		return "<div id='infoBar'>
					$info
				</div>";
	}

	// Retunera navigations-delen av sidan vid login
	public function getLoginNavigation($codarId, $current, $isAdmin = false) {

		// Visa bild för aktivt menyalternativ
		$currentImg = "<img src='Common/img/arrowdown.png' />";
		$tCurr = "";
		$cCurr = "";
		$aCurr = "";
		$sCurr = "";
		$adCurr = "";
		
		switch($current) {
			case \view\Navigation::UserViewsTraining:
				$tCurr = $currentImg;
			break;

			case \view\Navigation::UserViewsCodar:
				$cCurr = $currentImg;
			break;

			case \view\Navigation::UserViewsAttacks:
				$aCurr = $currentImg;
			break;

			case \view\Navigation::UserViewsShop:
				$sCurr = $currentImg;
			break;

			case \view\Navigation::AdminView:
				$adCurr = $currentImg;
			break;
		}

		$adminNav = "";

		// Admin nav
		if($isAdmin) {
			$adminNav = "<li><a href='".\view\Navigation::generateAdminGet()."'>Admin</a>$adCurr</li>";
		}

		return "<div id='navigation'>
				<ul>
					<li><a href='".\view\Navigation::generateCodarGet().$codarId."'>Min Codar</a>$cCurr</li>
					<li><a href='".\view\Navigation::generateTrainingGet()."'>Träning</a>$tCurr</li>
					<li><a href='".\view\Navigation::generateAttackGet()."'>Attacker</a>$aCurr</li>
					<li><a href='".\view\Navigation::generateShopGet()."'>Shop</a>$sCurr</li>
					$adminNav
				</ul>

				</div>";
	}

	// Hämta ut main-delen av sidan
	public function getPage($main, $message) {
		  	return"	<div role='main' id='main'>
		  					$message
  							$main
  						</div>
  					<footer>
  						<p class='footer'>2012 somfan development | somfan.nu | admin@codar.se | admin@somfan.nu</p>
  						<p class='quote'>\"Nonsense, Velvel. Blessed is the Lord. Good riddance to evil.\"</p>
  					</footer>

  					<!-- JavaScript at the bottom for fast page loading -->
  					<script src='./js/jquery-1.7.1.js'></script>
  					<script src='./js/script.js'></script>
  					<!-- end scripts -->
					</body>
				</html>";
	}

	// Hämta ut full sida
	public function getFullHTML5Page($title, $body) {

		return "<!doctype html>
				<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
				<!--[if lt IE 7]> <html class='no-js lt-ie9 lt-ie8 lt-ie7' lang='en'> <![endif]-->
				<!--[if IE 7]>    <html class='no-js lt-ie9 lt-ie8' lang='en'> <![endif]-->
				<!--[if IE 8]>    <html class='no-js lt-ie9' lang='en'> <![endif]-->
				<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
				<!--[if gt IE 8]><!--> <html class='no-js' lang='en'> <!--<![endif]-->
				<head>
  					<meta charset='utf-8'>

  					<title>$title</title>
  					<meta name='description' content=''>

  					<link rel='stylesheet' href='css/style.css'>

				</head>
					<body>
 						<header>
 							<img src='Common/img/header.png' />
  						</header>
  						<div role='main' id='main'>
  							$body
  						</div>
  					<footer>

  					</footer>

  					<!-- JavaScript at the bottom for fast page loading -->
  					<script src='./js/jquery-1.7.1.js'></script>
  					<script src='./js/script.js'></script>
  					<!-- end scripts -->
					</body>
				</html>";
	}
}

// Sätt style och hämta style
class Apperence {
	const STYLE1 = "style";
	const STYLE2 = "altstyle";
	const CSS = "css";

	public static function setStyle($style) {
		unset($_SESSION[self::CSS]);
		$_SESSION[self::CSS] = $style;
	}

	public static function getStyle() {
		return isset($_SESSION[self::CSS]) == true ? $_SESSION[self::CSS] : self::STYLE1;
	}
}