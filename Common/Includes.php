<?php
require_once('View/MasterView.php');
require_once('Controller/BaseController.php');

require_once('Common/Encrypt.php');
require_once('Common/PageView.php');
require_once('Common/Strings.php');
require_once('Common/Validation.php');

require_once('Config/Config.php');

require_once('Controller/AdminC.php');
require_once('Controller/AttackController.php');
require_once('Controller/CodarController.php');
require_once('Controller/ForgotPasswordController.php');
require_once('Controller/LoginController.php');
require_once('Controller/MainController.php');
require_once('Controller/MasterC.php');
require_once('Controller/MessageController.php');
require_once('Controller/RegisterUserC.php');
require_once('Controller/ShopController.php');
require_once('Controller/TrainingController.php');
require_once('Controller/ValidMailController.php');

require_once('DBAccess/CodarDAL.php');
require_once('DBAccess/Database.php');
require_once('DBAccess/ItemDAL.php');
require_once('DBAccess/MessageDAL.php');
require_once('DBAccess/Settings.php');
require_once('DBAccess/TrainingDAL.php');
require_once('DBAccess/UserDAL.php');

/* OBS Ska inkluderas lokalt
require_once('Install/InstallController.php');
require_once('Install/InstallHandler.php');
require_once('Install/InstallView.php');
*/

require_once('Model/AdminH.php');
require_once('Model/AttackHandler.php');
require_once('Model/Codar.php');
require_once('Model/CodarHandler.php');
require_once('Model/ForgotPasswordHandler.php');
require_once('Model/Item.php');
require_once('Model/Level.php');
require_once('Model/Lock.php');
require_once('Model/LoginHandler.php');
require_once('Model/MainHandler.php');
require_once('Model/Message.php');
require_once('Model/MessageHandler.php');
require_once('Model/Money.php');
require_once('Model/RegisterUserH.php');
require_once('Model/ShopHandler.php');
require_once('Model/Training.php');
require_once('Model/TrainingHandler.php');
require_once('Model/User.php');
require_once('Model/ValidMailHandler.php');

/* OBS Ska inkluderas lokalt 
require_once('Test/TestLogin.php');
require_once('Test/TestDatabase.php');
require_once('Test/TestAttack.php');
require_once('Test/TestLevel.php');
require_once('Test/TestLock.php');
*/


require_once('View/AdminView.php');
require_once('View/AttackView.php');
require_once('View/CodarView.php');
require_once('View/ForgotPasswordView.php');
require_once('View/LoginView.php');
require_once('View/MainView.php');
require_once('View/MessageView.php');
require_once('View/Navigation.php');
require_once('View/RegisterUser.php');
require_once('View/ShopView.php');
require_once('View/TrainingView.php');
require_once('View/ValidMailView.php');
require_once('View/StandardView.php');

require_once('Error.php');