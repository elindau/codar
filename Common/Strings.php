<?php
namespace Common;

class Strings {

	/**
	* Appspecifika
	*/
	const m_title = "Codar";
	const APP_NAME = "Codar";
	const GENERIC_ERROR_PAGE = "/Pages/GenericError.html";
	const GENERIC_LOST_PAGE = "/Pages/Lost.html";
	const ADMIN_MAIL = "admin@codar.se";

	// Typer, från gettype ex.
	const t_array = "array";

	// Lyckade förfaranden
	const m_saveFile = "Filuppladdning lyckades!";
	const m_logout = "Du är utloggad";
	const m_login = "Du är inloggad";
	const s_register = "Ny användare registrerad!";
	const s_delete = "Du har tagit bort användaren!";
	const s_activate = "Lyckad aktivering!";
	const s_pwreset = "Du har nu fått ett mail med dina uppgifter!";
	const s_createcodar = "Du har nu skapat din Codar!";

	/**
	* Felmeddelanden
	*/
	const e_wrongUserOrPw = "Fel användarnamn eller lösenord";
	const e_notamap = "Finns ingen map med det namnet";
	const e_saveFile = "Filuppladdningen misslyckades!";
	const e_sessionNotStarted = "Session har inte startat";
	const e_illegalExtension = "Fel filtyp!";
	const e_fileSave = "Gick inte spara filen";
	const e_maxFileSize = "Filen är för stor";
	const e_maxFileSize2 = "Filen är för stor2";
	const e_partialUpload = "Filen laddades inte upp helt";
	const e_noFile = "Ingen fil blev uppladdad";
	const e_noTmpFolder = "Något gick fel på servern";
	const e_saving = "Kunde inte spara filen";
	const e_uploadDest = "Ingen uppladdad fil";

	const e_pwMatch = "Lösenorden stämmer inte överrens";
	const e_pwFormat = "Lösenordet måste innehålla minst en stor bokstav och minst en siffra";
	const e_htmlFormat = "Förbjudna <html>-taggar";
	const e_usernameFormat = "Användarnamnet får bara innehålla tecken";
	const e_emailFormat = "Ogiltlig mail";
	const e_usernameExists = "Användarnamnet finns redan";
	const e_mailExists = "Mailadressen finns redan";
	const e_delete = "Det gick inte att ta bort användaren!";
	const e_database = "Det gick inte ansluta till databasen";
	const e_nameFormat = "Namnet får bara innehålla tecken";
	const e_failedInsert = "Något gick fel";

	// Aktivering
	const e_alreadyActive = "Användaren är redan aktiverad!";
	const e_wrongId = "Finns ingen användare att aktviera!";
	const e_couldntActivate = "Kunde inte aktivera användaren!";

	// Glömt lösenord
	const e_failedtoSendEmail = "Kunde inte skicka mail!";
	const e_noEmail = "Kunde inte hitta en användare med den mailen!";

	// Codar
	const e_noCodar = "Kunde inte hitta någon Codar";

	// Lås
	const e_lockedCodar = "Din Codar är fortfarande på träning!";

	// Filsträngar
	const f_name = "name";
	const f_tmp_name = "tmp_name";
	const f_type = "type";
	const f_size = "size";
	const f_error = "error";

	// Uppladdningskatalog
	const m_uploadmap = "./uploads/";

	/**
	* Arrayer
	*/
}