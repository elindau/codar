<?php
namespace Common;

class Encrypt {
	
	// Blowfish-salt
	private $m_blow = "$2a$07dromplo$";

	/**
	* Krypterar lösenord
	*
	* @param String $pw
	* @return String
	*/
	public function encryptPassword($pw) {
		return crypt($pw, $this->m_blow);
	}
}