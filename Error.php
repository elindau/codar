<?php

class Error {

	public static function myErrHandler($code, $message, $errFile, $errLine) { 
		// Meddelande som loggas
		$subject = "$message: PROBLEM vid " . \Common\Strings::APP_NAME . ': ' . date("F j, Y, g:i a"); 
		$body = "errFile: $errFile\r\n" . "errLine: $errLine\r\n";

		// För framtida bruk
		// mail(ADMIN_EMAIL_ADDR,$subject,$body); 

		// Logga felet
	  	error_log("$subject\r\n $body");  

	  	// Redirecta till generisk felsida
	 	header ("Location: http://{$_SERVER['HTTP_HOST']}/". \Common\Strings::GENERIC_ERROR_PAGE ); 
	  	exit; 
	} 
}