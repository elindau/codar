<?php
session_start();

require_once('Common/Includes.php');

 class MasterTest {

  public static function execTesting() {

    
    $page = new \Common\PageView();
    $db = new \DBAccess\Database();
    $body = "";

    $db->connect(new \DBAccess\Settings());
    $testDb = new \test\TestDatabase($db);
    $testLogin = new \test\TestLogin($db);
    $testAttack = new \test\TestAttack($db);
    $testLevel = new \test\TestLevel($db);
    $testLock = new \test\TestLock($db);
    
   
    if($testLogin->TestLogin()) {
      $body .= "<p>Logintest OK!</p>";
    } else {
      $body .= "<p>Logintest misslyckades</p>";
    }

    if($testDb->testUserDAL()) {
      $body .= "<p>Databastest för användare OK!</p>";
    } else {
      $body .= "<p>Databastest för användare misslyckades</p>";
    }

    if($testDb->testCodarDAL()) {
        $body .= "<p>Databastest för codar OK!</p>";
    } else {
        $body .= "<p>Databastest för codar misslyckades</p>";
    }

    if($testAttack->testAttackHandler()) {
        $body .= "<p>Attacktest OK!</p>";
        $body .= $testAttack->getMatchResult();
    } else {
        $body .= "<p>Attacktest misslyckades!</p>";
    }

    if($testLevel->testLevelHandler()) {
        $body .= "<p>Leveltest OK!</p>";
    } else {
        $body .= "<p>Leveltest misslyckades!</p>";
    }

    if($testLevel->testGameLostHandler()) {
        $body .= "<p>Leveltest2 (delevling) OK!</p>";
    } else {
        $body .= "<p>Leveltest2 (delevling) misslyckades!</p>";
    }

    if($testLock->testLockHandler()) {
        $body .= "<p>Låstest OK!</p>";
    } else {
        $body .= "<p>Låstest misslyckades!</p>";
    }   
 

    $db->close();
    $title = "Test";

    $site = $page->getFullHTML5Page($title, $body);

    return $site;
  }

}

echo MasterTest::execTesting();