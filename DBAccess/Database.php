<?php
namespace DBAccess;

class Database {

	private $m_mysqli = null;
	private $m_insertId = null;

	// Retunerar insert-idt
	public function getInsertId() {
		return $this->m_insertId;
	}

	// Försök ansluta till databasen
	public function connect(Settings $settings) {
		
		//$this->m_mysqli = new \mysqli($settings->m_host, $settings->m_user2, $settings->m_pass2, $settings->m_db);
		$this->m_mysqli = new \mysqli($settings->b_host, $settings->b_user, $settings->b_pass, $settings->b_db, $settings->b_port);

		if(mysqli_connect_error()) {
			trigger_error(\Common\Strings::e_database);
		}

		$this->m_mysqli->set_charset("utf8");
		
		return true;
	}

	// Försök stänga databasen
	public function close() {
		return $this->m_mysqli->close();
	}

	// Försök förbereda sql-frågan
	public function prepare($sql) {
		$stmt = $this->m_mysqli->prepare($sql);

		if(!($stmt)) {
			$this->throwError();
		}
		return $stmt;
	}

	// Förbereder och binder parametrarna
	public function paramPrepare($sql, $equals, $paramTypes) {

		$stmt = $this->prepare($sql);
		// Limmar ihop typerna och försöker binda
		// Samt kallar på bind_param med arrayer
		$params = array_merge(array(implode("", $paramTypes)), $equals);
		call_user_func_array(array($stmt, "bind_param"), $this->makeValuesReferenced($params));

		if(!($stmt)) {
			$this->throwError();
		}

		return $stmt;
	}

	// Gör värden till referenser
	private function makeValuesReferenced(&$array) {

		$refs = array();
		foreach($array as $key => $value) {
			$refs[$key] = &$array[$key];
		}
		return $refs;
	}

	// Tar emot godtyckligt antal argument och hämtar ut objekt
	public function paramselect($sql, $equals, $paramTypes) {
		$stmt = $this->paramPrepare($sql, $equals, $paramTypes);
		return $this->fetchObject($stmt);
	}

	// Tar emot godtyckligt antal argument och exekverar
	public function preparedQuery($sql, $values, $paramTypes) {
		$stmt = $this->paramPrepare($sql, $values, $paramTypes);
		$ret = $stmt->execute();

		// Måste spara undan insert-idt
		$this->m_insertId = $this->m_mysqli->insert_id;

		$stmt->close();
		return $ret;
	}

	/**
	* Hämtar ut objekt
	*
	* @param Statement $stmt
	* @return stdObject $object
	*/
	public function fetchObject($stmt) {
		$result = $stmt->execute();
		$result = $stmt->get_result();

		// Använder stdClass
		$object = $result->fetch_object();

		// Blev det träff?
		return $object == null ? false : $object;
	}

	/**
	* Vanlig select, hantering i DAL-klass
	* 
	* @param String $qry
	* @return String $stmt
	*/
	public function select($qry) {
		$stmt = $this->prepare($qry);

		if(!($stmt->execute())) {
			$this->throwError();
		}

		return $stmt;
	}

	/**
	* Kastar fel beroende på m_mysli-objektet
	*
	* @throws Exception
	*/
	public function throwError() {
		throw new \Exception("(" . $this->m_mysqli->errno . ") " . $this->m_mysqli->error);
	}
}