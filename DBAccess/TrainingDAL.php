<?php

namespace DBAccess;

class TrainingDAL {

	// Tabell-konstanter
	const TABLE_TRAINING = "training";
	const COL_TRAINING_ID = "trainingId";
	const COL_TRAINING_ID_TYPE = "i";
	const COL_TRAINING_NAME = "name";
	const COL_TRAINING_NAME_TYPE = "s";
	const COL_TRAINING_VALUE = "value";
	const COL_TRAINING_VALUE_TYPE = "i";
	const COL_TRAINING_TIMELOCK = "timelock";
	const COL_TRAINING_TIMELOCK_TYPE = "i";

	const TABLE_ATRAINING = "availtraining";
	const COL_ATRAINING_ID = "atId";
	const COL_ATRAINING_ID_TYPE = "i";
	const COL_ATRAINING_CODAR_ID = "codarId";
	const COL_ATRAINING_CODAR_ID_TYPE = "i";
	const COL_ATRAINING_TRAINING_ID = "trainingId";
	const COL_ATRAINING_TRAINING_ID_TYPE = "i";

	private $m_db = null;

	public function __construct($db) {
		$this->m_db = $db;
	}

	/**
	* Hämtar ut alla tränings-typer med vanlig select
	* 
	* @return array \model\Training
	*/
	public function selectAllTraining() {

		$sql = "SELECT * FROM " . TrainingDAL::TABLE_TRAINING;
		$stmt = $this->m_db->select($sql);

		if(!($stmt->bind_result($id, $name, $value, $timelock))) {
			throw new \Exception();
		}

		$ret = array();

		while($stmt->fetch()) {
			$training = new \model\Training($id, $name, $value, $timelock);
			$ret[] = $training;
		}

		$stmt->close();

		return $ret;
	}

	/**
	 * Hämta ut all träning som finns på en viss Codar
	 * @param int $codarId 
	 * @return array \model\Training
	 */
	public function selectAllAvailableTraining($codarId) {
		// Hämta ut all träning med ett visst codarId
		$sql = "SELECT * FROM " . TrainingDAL::TABLE_ATRAINING . " WHERE " . TrainingDAL::COL_ATRAINING_CODAR_ID . " = ?";

		$stmt = $this->m_db->prepare($sql);
		if(!($stmt)) {
			throw new \Exception();
		}
		$stmt->bind_param('i', $codarId);
		$stmt->execute();
		$stmt->bind_result($atId, $cId, $tId);

		// Alla idn
		$trainingIds = array();
		while($stmt->fetch()) {
			$trainingIds[] = $tId;
		}

		$stmt->close();

		// Hämta ut all träning
		$trainings = $this->selectAllTraining();
		$retTrainings = array();
		// Där idt stämmer överrens..
		foreach($trainings as $training) {
			foreach($trainingIds as $id) {
				if($training->getTrainingId() == $id) {
					// .. och lägg till
					$retTrainings[] = $training;
				}
			}
		}

		return $retTrainings;
	}

	/**
	* Hämta ut Träningstyp med godtyckligt antal argument
	*
	* @param array $arguments
	* @param array $equals
	* @param array $types
	* @return \model\Training | boolean false
	*/
	public function selectTrainingBy($arguments, $equals, $types) {

		// select-fråga som lägger in alla argument
		$sql = "SELECT * FROM " . TrainingDAL::TABLE_TRAINING . " WHERE (";
		for($i = 0; $i < count($arguments); $i++) {
			if($i == count($arguments) - 1) {
				$sql .= $arguments[$i] . " = ?)";
			} else {
				$sql .= $arguments[$i] . " = ? && ";
			}
		}

		// Får tillbaka ett objekt av stdClass
		$genObject = $this->m_db->paramselect($sql, $equals, $types);

		// Om det inte hittades något retunera, annars...
		if(!($genObject)) {
			return false;
		}

		// ... Skapa en ny Träning och retunera denna
		$training = new \model\Training($genObject->trainingId, 
								$genObject->name, 
								$genObject->value, 
								$genObject->timelock);
		
		return $training;
	}

	/**
	* Lägg till tillgänglig träning i databas
	*
	* @param int $trainingId
	* @param int $codarId
	* @return boolean
	*/
	public function insertAvailableTraining($trainingId, $codarId) {

		// Skapar en insert-fråga till tabellen user
		$sql = "INSERT INTO " . TrainingDAL::TABLE_ATRAINING . "(" 
														. TrainingDAL::COL_ATRAINING_CODAR_ID . ", " 
														. TrainingDAL::COL_ATRAINING_TRAINING_ID . 
													") VALUES(?, ?)";
		
		// Får tillbaka true eller false beroende på inserten
		$equals = array($codarId, $trainingId);
		$types = array(TrainingDAL::COL_ATRAINING_CODAR_ID_TYPE, TrainingDAL::COL_ATRAINING_TRAINING_ID_TYPE);
		return $this->m_db->preparedQuery($sql, $equals, $types);
	}

	/**
	* Lägg till uppstartsträning i databas
	*
	* @param int $codarId
	* @return boolean
	*/
	public function insertStartupTraining($codarId) {

		// Skapar en insert-fråga till tabellen user
		$sql = "INSERT INTO " . TrainingDAL::TABLE_ATRAINING . "(" 
														. TrainingDAL::COL_ATRAINING_CODAR_ID . ", " 
														. TrainingDAL::COL_ATRAINING_TRAINING_ID . 
													") VALUES(?, ?)";
		
		// Får tillbaka true eller false beroende på inserten
		$equals = array($codarId, 1);
		$types = array(TrainingDAL::COL_ATRAINING_CODAR_ID_TYPE, TrainingDAL::COL_ATRAINING_TRAINING_ID_TYPE);
		return $this->m_db->preparedQuery($sql, $equals, $types);
	}

	/**
	 * Ta bort all träning från en codar
	 * @param int $codarId 
	 * @return boolean
	 */
	public function deleteAllFromAtraining($codarId) {
		$sql = "DELETE FROM " . TrainingDAL::TABLE_ATRAINING . " WHERE " . TrainingDAL::COL_ATRAINING_CODAR_ID . " = ?";
		return $this->m_db->preparedQuery($sql, array($codarId), array(TrainingDAL::COL_ATRAINING_CODAR_ID));
	}
}