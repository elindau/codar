<?php

namespace DBAccess;

class UserDAL {

	// Tabell-konstanter
	const TABLE_USER = "login";
	const COL_USER_ID = "userId";
	const COL_USER_ID_TYPE = "i";
	const COL_USER_NAME = "name";
	const COL_USER_NAME_TYPE = "s";
	const COL_USER_PASSWORD = "password";
	const COL_USER_PASSWORD_TYPE = "s";
	const COL_USER_EMAIL = "email";
	const COL_USER_EMAIL_TYPE = "s";
	const COL_USER_TOKEN = "token";
	const COL_USER_TOKEN_TYPE = "s";
	const COL_USER_EMAILA = "emailActive";
	const COL_USER_EMAILA_TYPE = "s";
	const COL_USER_ACTIVE = "active";
	const COL_USER_ACTIVE_TYPE = "i";
	const COL_USER_ROLE = "role";
	const COL_USER_ROLE_TYPE = "i";
	const COL_USER_CODAR_ID = "codarId";
	const COL_USER_CODAR_ID_TYPE = "i";
	
	const CLASS_USER = "\model\User";

	private $m_db = null;

	public function __construct($db) {
		$this->m_db = $db;
	}

	/**
	* Hämta ut användare med godtyckligt antal argument
	*
	* @param array $arguments
	* @param array $equals
	* @param array $types
	* @return \model\User | boolean false
	*/
	public function selectUserBy($arguments, $equals, $types) {

		// select-fråga som lägger in alla argument
		$sql = "SELECT * FROM " . UserDAL::TABLE_USER . " WHERE (";
		for($i = 0; $i < count($arguments); $i++) {
			if($i == count($arguments) - 1) {
				$sql .= $arguments[$i] . " = ?)";
			} else {
				$sql .= $arguments[$i] . " = ? && ";
			}
		}

		// Får tillbaka ett objekt av stdClass
		$genObject = $this->m_db->paramselect($sql, $equals, $types);

		// Om det inte hittades något retunera, annars...
		if(!($genObject)) {
			return false;
		}

		// ... Skapa en ny användare och retunera denna
		$user = new \model\User($genObject->userId, 
								$genObject->name, 
								$genObject->password, 
								$genObject->email, false);
		// Är det en admin?
		if($genObject->role == \model\User::ADMIN) {
			$user->setRole(\model\User::ADMIN);
		}

		// Finns det en token?
		if($genObject->token != null) {
			$user->setToken($genObject->token);
		}

		// Är användaren aktiv?
		if($genObject->active == \model\User::ACTIVE) {
			$user->setActive(\model\User::ACTIVE);
		}

		// Finns det en mail-länk?
		if($genObject->emailActive != null) {
			$user->setMailId($genObject->emailActive);
		}

		// Är det inte första gången
		if($genObject->codarId != \model\User::FIRST_LOGIN) {
			$user->setCodarId($genObject->codarId);
		}
		
		return $user;
	}

	/**
	* Hämtar ut alla användare med vanlig select
	* 
	* @return array \model\User
	*/
	public function selectAllUsers() {

		$sql = "SELECT * FROM " . UserDAL::TABLE_USER;
		$stmt = $this->m_db->select($sql);

		if(!($stmt->bind_result($id, $name, $pw, $mail, $token, $emailActive, $active, $role, $codarId))) {
			throw new \Exception();
		}

		$ret = array();

		while($stmt->fetch()) {
			$user = new \model\User($id, $name, $pw, $mail);
			$user->setRole($role);
			$ret[] = $user;
		}

		$stmt->close();

		return $ret;
	}

	/**
	* Lägg till användare i databas
	*
	* @param \model\User $user
	* @return boolean
	*/
	public function insertUser(\model\User $user) {

		// Skapar en insert-fråga till tabellen user
		$sql = "INSERT INTO " . UserDAL::TABLE_USER . "(" 
														. UserDAL::COL_USER_NAME . ", " 
														. UserDAL::COL_USER_PASSWORD . ", " 
														. UserDAL::COL_USER_EMAIL . ","
														. UserDAL::COL_USER_TOKEN . ","
														. UserDAL::COL_USER_EMAILA .
													") VALUES(?, ?, ?, ?, ?)";
		
		// Får tillbaka true eller false beroende på inserten
		$this->m_db->preparedQuery($sql, array($user->getUsername(), $user->getPassword(), 
			$user->getEmail(), $user->getToken(), $user->getMailId()), 
												array(UserDAL::COL_USER_NAME_TYPE, UserDAL::COL_USER_PASSWORD_TYPE, 
													UserDAL::COL_USER_EMAIL_TYPE, UserDAL::COL_USER_TOKEN_TYPE, 
													UserDAL::COL_USER_EMAILA_TYPE));
		return $this->m_db->getInsertId();
	}

	/**
	* Uppdatera användare i databas
	*
	* @param \model\User $user
	* @param array() $arguments Vilka fält som ska uppdateras
	* @param array() $equals Till vilka värden
	* @param array() $types
	*
	* @return boolean
	*/
	public function updateUser(\model\User $user, $arguments, $equals, $types) {

		// Insert-fråga som lägger in alla argument
		$sql = "UPDATE " . UserDAL::TABLE_USER . " SET ";
		for($i = 0; $i < count($arguments); $i++) {
			if($i == count($arguments) - 1) {
				$sql .= $arguments[$i] . " = ?";
			} else {
				$sql .= $arguments[$i] . " = ?, ";
			}
		}
		$sql .= " WHERE " . UserDAL::COL_USER_ID . " = ?";

		// Lägg till userId för att binda
		$equals[] = $user->getUserId();
		$types[] = UserDAL::COL_USER_ID_TYPE;

		// Kör inserten
		return $this->m_db->preparedQuery($sql, $equals, $types);
	}

	/**
	* Ta bort användare från databas
	*
	* @param Integer $userId
	* @return boolean
	*/
	public function deleteUser($userId) {

		$sql = "DELETE FROM " . UserDAL::TABLE_USER . " WHERE " . UserDAL::COL_USER_ID . " = ?";
		return $this->m_db->preparedQuery($sql, array($userId), array(UserDAL::COL_USER_ID_TYPE));
	}
}