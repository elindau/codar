<?php

namespace DBAccess;

class MessageDAL {

	// Tabell-konstanter
	const TABLE_MESSAGE = "message";
	const COL_MESSAGE_ID = "messageId";
	const COL_MESSAGE_ID_TYPE = "i";
	const COL_MESSAGE_CODAR_ID = "codarId";
	const COL_MESSAGE_CODAR_ID_TYPE = "i";
	const COL_MESSAGE_MESSAGE = "message";
	const COL_MESSAGE_MESSAGE_TYPE = "s";

	private $m_db = null;

	public function __construct($db) {
		$this->m_db = $db;
	}

	/**
	* Hämta ut Meddelanden för en viss kodar
	*
	* @param int $codarId
	* @return array \model\Message | boolean false
	*/
	public function selectMessages($codarId) {

		// Hämta ut alla meddelanden med ett visst codarId
		$sql = "SELECT * FROM " . MessageDAL::TABLE_MESSAGE . " WHERE " . MessageDAL::COL_MESSAGE_CODAR_ID . " = ?";

		$stmt = $this->m_db->prepare($sql);
		if(!($stmt)) {
			throw new \Exception();
		}
		$stmt->bind_param('i', $codarId);
		$stmt->execute();
		$stmt->bind_result($mId, $cId, $message);

		// Alla idn
		$messages = array();
		while($stmt->fetch()) {
			$messages[] = new \model\Message($mId, $cId, $message);
		}

		$stmt->close();

		return $messages;
	}

	/**
	* Lägg till ett meddelande i databasen
	*
	* @param \model\Message $message
	* @return boolean
	*/
	public function insertMessage(\model\Message $message) {

		// Skapar en insert-fråga till tabellen item
		$sql = "INSERT INTO " . MessageDAL::TABLE_MESSAGE . "(" 
														. MessageDAL::COL_MESSAGE_CODAR_ID . ", " 
														. MessageDAL::COL_MESSAGE_MESSAGE .
													") VALUES(?, ?)";
		
		// Får tillbaka true eller false beroende på inserten
		$equals = array($message->getMessageCodarId(), $message->getMessage());
		$types = array(MessageDAL::COL_MESSAGE_CODAR_ID_TYPE, MessageDAL::COL_MESSAGE_MESSAGE_TYPE);
		return $this->m_db->preparedQuery($sql, $equals, $types);
	}

	/**
	* Ta bort meddelande från databas
	*
	* @param Integer $messageId
	* @return boolean
	*/
	public function deleteMessage($messageId) {

		$sql = "DELETE FROM " . MessageDAL::TABLE_MESSAGE . " WHERE " . MessageDAL::COL_MESSAGE_ID . " = ?";
		return $this->m_db->preparedQuery($sql, array($messageId), array(MessageDAL::COL_MESSAGE_ID_TYPE));
	}
}