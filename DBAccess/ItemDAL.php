<?php

namespace DBAccess;

class ItemDAL {

	// Tabell-konstanter
	const TABLE_ITEM = "item";
	const COL_ITEM_ID = "itemId";
	const COL_ITEM_ID_TYPE = "i";
	const COL_ITEM_NAME = "name";
	const COL_ITEM_NAME_TYPE = "s";
	const COL_ITEM_COST = "cost";
	const COL_ITEM_COST_TYPE = "i";
	const COL_ITEM_FILENAME = "filename";
	const COL_ITEM_FILENAME_TYPE = "s";
	const COL_ITEM_DESCR = "descr";
	const COL_ITEM_DESCR_TYPE = "s";
	const COL_ITEM_VALUE = "value";
	const COL_ITEM_VALUE_TYPE = "i";

	private $m_db = null;

	public function __construct($db) {
		$this->m_db = $db;
	}

	/**
	* Hämtar ut alla items med vanlig select
	* 
	* @return array \model\Item
	*/
	public function selectAllItems() {

		$sql = "SELECT * FROM " . ItemDAL::TABLE_ITEM;
		$stmt = $this->m_db->select($sql);

		if(!($stmt->bind_result($id, $name, $cost, $filename, $descr, $value))) {
			throw new \Exception();
		}

		$ret = array();

		while($stmt->fetch()) {
			$item = new \model\Item($id, $name, $cost, $filename, $descr, $value);
			$ret[] = $item;
		}

		$stmt->close();

		return $ret;
	}

	/**
	* Hämta ut Item med godtyckligt antal argument
	*
	* @param array $arguments
	* @param array $equals
	* @param array $types
	* @return \model\Item | boolean false
	*/
	public function selectItemBy($arguments, $equals, $types) {

		// select-fråga som lägger in alla argument
		$sql = "SELECT * FROM " . ItemDAL::TABLE_ITEM . " WHERE (";
		for($i = 0; $i < count($arguments); $i++) {
			if($i == count($arguments) - 1) {
				$sql .= $arguments[$i] . " = ?)";
			} else {
				$sql .= $arguments[$i] . " = ? && ";
			}
		}

		// Får tillbaka ett objekt av stdClass
		$genObject = $this->m_db->paramselect($sql, $equals, $types);

		// Om det inte hittades något retunera, annars...
		if(!($genObject)) {
			return false;
		}

		// ... Skapa ett nytt Item och retunera detta
		$item = new \model\Item($genObject->itemId, $genObject->name, $genObject->cost, $genObject->filename, $genObject->descr, $genObject->value);
		
		return $item;
	}

	/**
	* Lägg till ett item i databasen
	*
	* @param \model\Item $item
	* @return boolean
	*/
	public function insertItem(\model\Item $item) {

		// Skapar en insert-fråga till tabellen item
		$sql = "INSERT INTO " . ItemDAL::TABLE_ITEM . "(" 
														. ItemDAL::COL_ITEM_NAME . ", " 
														. ItemDAL::COL_ITEM_COST . ", " 
														. ItemDAL::COL_ITEM_FILENAME . ","
														. ItemDAL::COL_ITEM_DESCR . ","
														. ItemDAL::COL_ITEM_VALUE .
													") VALUES(?, ?, ?, ?, ?)";
		
		// Får tillbaka true eller false beroende på inserten
		$equals = array($item->getName(), $item->getCost(), $item->getFilename(), $item->getDescr(), $item->getValue());
		$types = array(ItemDAL::COL_ITEM_NAME_TYPE, ItemDAL::COL_ITEM_COST_TYPE, ItemDAL::COL_ITEM_FILENAME_TYPE, ItemDAL::COL_ITEM_DESCR_TYPE, ItemDAL::COL_ITEM_VALUE_TYPE);
		return $this->m_db->preparedQuery($sql, $equals, $types);
	}

	/**
	* Ta bort item från databas
	*
	* @param Integer $itemId
	* @return boolean
	*/
	public function deleteItem($itemId) {

		$sql = "DELETE FROM " . ItemDAL::TABLE_ITEM . " WHERE " . ItemDAL::COL_ITEM_ID . " = ?";
		return $this->m_db->preparedQuery($sql, array($itemId), array(ItemDAL::COL_ITEM_ID_TYPE));
	}
}