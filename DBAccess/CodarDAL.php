<?php

namespace DBAccess;

class CodarDAL {

	// Tabell-konstanter
	const TABLE_CODAR = "codar";
	const COL_CODAR_ID = "codarId";
	const COL_CODAR_ID_TYPE = "i";
	const COL_CODAR_NAME = "name";
	const COL_CODAR_NAME_TYPE = "s";
	const COL_CODAR_LEVEL = "level";
	const COL_CODAR_LEVEL_TYPE = "i";
	const COL_CODAR_EXP = "exp";
	const COL_CODAR_EXP_TYPE = "i";
	const COL_CODAR_LANGUAGE_ID = "languageId";
	const COL_CODAR_LANGUAGE_ID_TYPE = "i";
	const COL_CODAR_TRAINING_ID = "trainingId";
	const COL_CODAR_TRAINING_ID_TYPE = "i";
	const COL_CODAR_TIMELOCKED = "timeLocked";
	const COL_CODAR_TIMELOCKED_TYPE = "d";
	const COL_CODAR_MONEY = "money";
	const COL_CODAR_MONEY_TYPE = "i";
	const COL_CODAR_ENERGY = "energy";
	const COL_CODAR_ENERGY_TYPE = "i";

	const TABLE_LANGUAGE = "language";
	const COL_LANGUAGE_ID = "languageId";
	const COL_LANGUAGE_ID_TYPE = "i";
	const COL_LANGUAGE_NAME = "name";
	const COL_LANGUAGE_NAME_TYPE = "s";

	const CLASS_CODAR = "\model\Codar";

	private $m_db = null;

	public function __construct($db) {
		$this->m_db = $db;
	}

	/**
	* Hämta ut Codar med godtyckligt antal argument
	*
	* @param array $arguments
	* @param array $equals
	* @param array $types
	* @return \model\Codar | boolean false
	*/
	public function selectCodarBy($arguments, $equals, $types) {

		// select-fråga som lägger in alla argument
		$sql = "SELECT * FROM " . CodarDAL::TABLE_CODAR . " WHERE (";
		for($i = 0; $i < count($arguments); $i++) {
			if($i == count($arguments) - 1) {
				$sql .= $arguments[$i] . " = ?)";
			} else {
				$sql .= $arguments[$i] . " = ? && ";
			}
		}

		// Får tillbaka ett objekt av stdClass
		$genObject = $this->m_db->paramselect($sql, $equals, $types);

		// Om det inte hittades något retunera, annars...
		if(!($genObject)) {
			return false;
		}

		// ... Skapa en ny Codar och retunera denna
		$codar = new \model\Codar($genObject->codarId, 
								$genObject->name, 
								$genObject->level, 
								$genObject->exp,
								$genObject->languageId,
								$genObject->trainingId,
								$genObject->timeLocked,
								$genObject->money,
								$genObject->energy);
		
		return $codar;
	}

	/**
	* Hämtar ut alla codars med vanlig select
	* 
	* @return array \model\Codar
	*/
	public function selectAllCodars() {

		$sql = "SELECT * FROM " . CodarDAL::TABLE_CODAR;
		$stmt = $this->m_db->select($sql);

		if(!($stmt->bind_result($id, $name, $level, $exp, $languageId, $trainingId, $timeLocked, $money, $energy))) {
			throw new \Exception();
		}

		$ret = array();

		while($stmt->fetch()) {
			$codar = new \model\Codar($id, $name, $level, $exp, $languageId, $trainingId, $timeLocked, $money, $energy);
			$ret[] = $codar;
		}

		$stmt->close();

		return $ret;
	}

	/**
	* Hämtar ut alla tillgängliga språk
	*
	* @return $array String
	*/
	public function selectAllLanguages() {
		$sql = "SELECT * FROM " . CodarDAL::TABLE_LANGUAGE;
		$stmt = $this->m_db->select($sql);

		if(!($stmt->bind_result($id, $name))) {
			throw new \Exception();
		}

		$names = array();
		while($stmt->fetch()) {
			$names[$id] = $name;
		}

		$stmt->close();

		return $names;
	}

	/**
	* Lägg till codar i databas
	*
	* @param \model\Codar $codar
	* @return boolean
	*/
	public function insertCodar(\model\Codar $codar) {

		// Skapar en insert-fråga till tabellen user
		$sql = "INSERT INTO " . CodarDAL::TABLE_CODAR . "(" 
														. CodarDAL::COL_CODAR_NAME . ", " 
														. CodarDAL::COL_CODAR_LEVEL . ", " 
														. CodarDAL::COL_CODAR_EXP . ", "
														. CodarDAL::COL_CODAR_LANGUAGE_ID . ","
														. CodarDAL::COL_CODAR_MONEY .
													") VALUES(?, ?, ?, ?, ?)";
		
		// Får tillbaka true eller false beroende på inserten
		// Retunerar sen primärnyckeln där codarn lades till
		if($this->m_db->preparedQuery($sql, array($codar->getCodarName(), $codar->getCodarLevel(), $codar->getCodarExp(), $codar->getCodarLanguageId(), $codar->getMoney()), 
											array(CodarDAL::COL_CODAR_NAME_TYPE, CodarDAL::COL_CODAR_LEVEL_TYPE, CodarDAL::COL_CODAR_EXP_TYPE, CodarDAL::COL_CODAR_LANGUAGE_ID_TYPE, CodarDAL::COL_CODAR_MONEY_TYPE))) {
		return $this->m_db->getInsertId();
		}
		return false;
	}

	/**
	* Uppdatera användare i databas
	*
	* @param \model\Codar $codar
	* @param array() $arguments Vilka fält som ska uppdateras
	* @param array() $equals Till vilka värden
	* @param array() $types
	*
	* @return boolean
	*/
	public function updateCodar(\model\Codar $codar, $arguments, $equals, $types) {

		// Insert-fråga som lägger in alla argument
		$sql = "UPDATE " . CodarDAL::TABLE_CODAR . " SET ";
		for($i = 0; $i < count($arguments); $i++) {
			if($i == count($arguments) - 1) {
				$sql .= $arguments[$i] . " = ?";
			} else {
				$sql .= $arguments[$i] . " = ?, ";
			}
		}
		$sql .= " WHERE " . CodarDAL::COL_CODAR_ID . " = ?";

		// Lägg till codarId för att binda
		$equals[] = $codar->getCodarId();
		$types[] = CodarDAL::COL_CODAR_ID_TYPE;

		// Kör inserten
		return $this->m_db->preparedQuery($sql, $equals, $types);
	}

	/**
	* Ta bort codar från databas
	*
	* @param Integer $codarId
	* @return boolean
	*/
	public function deleteCodar($codarId) {

		$sql = "DELETE FROM " . CodarDAL::TABLE_CODAR . " WHERE " . CodarDAL::COL_CODAR_ID . " = ?";
		return $this->m_db->preparedQuery($sql, array($codarId), array(CodarDAL::COL_CODAR_ID_TYPE));
	}
}