<?php

namespace controller;

class InstallController {

	public function doControll(\view\InstallView $iv) {
		$installHandler = new \model\InstallHandler(new \DBAccess\Settings());
		$installView = $iv;

		// Försökte lägga till admin
		if($installView->triedToRegister()) {

			// Öppna upp databasen som vanligt
			$db = new \DBAccess\Database();
            $db->connect(new \DBAccess\Settings());

            $outcome = $installHandler->addAdmin($installView->getUsername(), 
                                        $installView->getPassword(), 
                                        $installView->getPasswordRepeat(), 
                                        $installView->getEmail(), 
                                        $db);

            if(is_array($outcome)) {
            	// Fel
            	foreach($outcome as $err) {
            		$installView->setError($err);
            	}
            } else if($outcome == true) {
            	// Nice
            	$installView->setSuccess("Lyckad initiering av databasen!");
            }

			$db->close();
		}

		// Avinstallering lokalt
		if($installView->isUninstallLocalSet()) {
			if($installHandler->deleteDatabase(true)) {
				$installView->setSuccess("Nice!");
			} else {
				$installView->setError("Gick inte att ta bort databasen!");
			}
		}

		// Avinstallering webb
		if($installView->isUninstallWebbset()) {
			if($installHandler->deleteDatabase(false)) {
				$installView->setSuccess("Nice!");
			} else {
				$installView->setError("Gick inte att ta bort databasen!");	
			}
		}

		if($installView->isLocalSet()) {
			// Lokalt valt
			$html = $installView->doInstallUninstallLocal();
		} else if ($installView->isWebbSet()) {
			// Webb valt
			$html = $installView->doInstallUninstallWebb();
		} else if ($installView->isInstallLocalSet()) {

			if($installHandler->createDatabase()) {
				// Databasen är skapad eller så finns den redan

				// Lägg till tabellerna
				if($installHandler->createTables(true)) {
					$installHandler->initTables();

				} else {
					trigger_error(\Common\Strings::e_database);
				}

			} else {
					// Kunde inte skapa databasen
					trigger_error(\Common\Strings::e_database);
			}
			$html = $installView->doAdminForm();	
		} else if($installView->isInstallWebbSet()) {
			// Binero - kan inte skapa databasen själv

			// Lägg till tabellerna
			if($installHandler->createTables(false)) {

				$installHandler->initTables();
			} else {
				trigger_error(\Common\Strings::e_database);
			}
			$html = $installView->doAdminForm();	
		} else {
			$html = $installView->doLocalWebb();
		}

		// html att visa
		
		return $html;
	}

}