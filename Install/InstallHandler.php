<?php

namespace model;

class InstallHandler {

  private $m_settings = null;
  private $m_mysqli = null;

  public function __construct(\DBAccess\Settings $settings) {
    $this->m_settings = $settings;
  }

  public function addAdmin($username, $pw, $pwrepeat, $email, $db) {

    $userDal = new \DBAccess\UserDAL($db);
    // Lägg till admin
    $registerHandler = new \model\RegisterUserH($db);
    $userId = $registerHandler->registerUser($username, $pw, $pwrepeat, $email);

    // Finns fel att skriva ut!
    if(is_array($userId)) {
        return $userId;
    }

    // Hämta uta användare
    $arguments = array(\DBAccess\UserDAL::COL_USER_ID);
    $equals = array($userId);
    $types = array(\DBAccess\UserDAL::COL_USER_ID_TYPE);
    $user = $userDal->selectUserBy($arguments, $equals, $types);

    // Gör till admin och aktiv direkt
    // Uppdatera
    $arguments = array(\DBAccess\UserDAL::COL_USER_ACTIVE, \DBAccess\UserDAL::COL_USER_ROLE);
    $equals = array(\model\User::ACTIVE, \model\User::ADMIN);
    $types = array(\DBAccess\UserDAL::COL_USER_ACTIVE_TYPE, \DBAccess\UserDAL::COL_USER_ROLE_TYPE);
    $userDal->updateUser($user, $arguments, $equals, $types);

    return true;

  }

  /**
   * Försöker skapa databasen
   * @return boolean
   */
  public function createDatabase() {

    $this->m_mysqli = new \mysqli($this->m_settings->m_host, $this->m_settings->m_user, $this->m_settings->m_pass);
    $this->m_mysqli->set_charset("utf8");

    if($this->m_mysqli->query(DatabaseSetup::createDatabaseSQL($this->m_settings->m_db))) {
        $this->m_mysqli->close();
        return true;

    } else {

        $this->m_mysqli->close();
        return false;
    }
  }

  public function deleteDatabase($local) {

    if($local) {
        // Ta bort både databas och tabeller
        $this->m_mysqli = new \mysqli($this->m_settings->m_host, $this->m_settings->m_user, $this->m_settings->m_pass, $this->m_settings->m_db);
        $db = $this->m_settings->m_db;

        if($this->m_mysqli->connect_errno) {
            return false;
        }

        if($this->m_mysqli->query("DROP DATABASE $db;")) {
            return true;
        } else {
            return false;
        }

    } else {
        // Ta bara bort tabeller
        $this->m_mysqli = new \mysqli($this->m_settings->b_host, $this->m_settings->b_user, $this->m_settings->b_pass, $this->m_settings->b_db, $this->m_settings->b_port);

        if($this->m_mysqli->connect_errno) {
            return false;
        }

        $this->m_mysqli->query("DROP TABLE 'codar';");
        $this->m_mysqli->query("DROP TABLE 'item';");
        $this->m_mysqli->query("DROP TABLE 'language';");
        $this->m_mysqli->query("DROP TABLE 'login';");
        $this->m_mysqli->query("DROP TABLE 'message';");
        $this->m_mysqli->query("DROP TABLE 'training';");
        $this->m_mysqli->query("DROP TABLE 'availtraining';");
    }
  }

  /**
   * Försöker skapa tabellerna
   * @return type
   */
  public function createTables($local) {

    if($local) {
        $this->m_mysqli = new \mysqli($this->m_settings->m_host, $this->m_settings->m_user, $this->m_settings->m_pass, $this->m_settings->m_db);
    } else {
        $this->m_mysqli = new \mysqli($this->m_settings->b_host, $this->m_settings->b_user, $this->m_settings->b_pass, $this->m_settings->b_db, $this->m_settings->b_port);
    }
    $this->m_mysqli->set_charset("utf8");
    

    if(!$this->m_mysqli->query(DatabaseSetup::CREATE_TABLE_CODAR)) {
        return false;
    }

    if(!$this->m_mysqli->query(DatabaseSetup::CREATE_TABLE_ITEM)) {
        return false;
    }

    if(!$this->m_mysqli->query(DatabaseSetup::CREATE_TABLE_LANGUAGE)) {
        return false;
    }

    if(!$this->m_mysqli->query(DatabaseSetup::CREATE_TABLE_LOGIN)) {
        return false;
    }

    if(!$this->m_mysqli->query(DatabaseSetup::CREATE_TABLE_MESSAGE)) {
        return false;
    }

    if(!$this->m_mysqli->query(DatabaseSetup::CREATE_TABLE_TRAINING)) {
        return false;
    }

    if(!$this->m_mysqli->query(DatabaseSetup::CREATE_TABLE_CODAR)) {
        return false;
    }

    if(!$this->m_mysqli->query(DatabaseSetup::CREATE_TABLE_AVAILTRAINING)) {
        return false;
    }

    return true;

  }

  // Initerar startvärden till de tabellerna som ska ha det
  public function initTables() {

    if($this->m_mysqli->query(DatabaseSetup::INSERT_INTO_LANGUAGE)) {

    }

    if($this->m_mysqli->query(DatabaseSetup::INSERT_INTO_TRAINING)) {

    }

    $this->m_mysqli->close();

    return true;
  }

}

class DatabaseSetup {

  const CREATE_DATABASE = "CREATE DATABASE IF NOT EXISTS `codar` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;";

  const CREATE_TABLE_AVAILTRAINING = "CREATE TABLE IF NOT EXISTS `availtraining` (
                                                                  `atId` int(11) NOT NULL AUTO_INCREMENT,
                                                                  `codarId` int(11) NOT NULL,
                                                                  `trainingId` int(11) NOT NULL,
                                                                  PRIMARY KEY (`atId`)
                                                                  ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;";

  const CREATE_TABLE_CODAR = "CREATE TABLE IF NOT EXISTS `codar` (
                                                        `codarId` int(11) NOT NULL AUTO_INCREMENT,
                                                        `name` varchar(45) NOT NULL,
                                                        `level` int(11) NOT NULL,
                                                        `exp` int(11) NOT NULL,
                                                        `languageId` int(11) NOT NULL,
                                                        `trainingId` int(11) NOT NULL DEFAULT '0',
                                                        `timeLocked` float DEFAULT NULL,
                                                        `money` int(11) NOT NULL DEFAULT '0',
                                                        `energy` int(11) NOT NULL DEFAULT '5',
                                                        PRIMARY KEY (`codarId`)
                                                        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;";

  const CREATE_TABLE_ITEM = "CREATE TABLE IF NOT EXISTS `item` (
                                                      `itemId` int(11) NOT NULL AUTO_INCREMENT,
                                                      `name` varchar(15) NOT NULL,
                                                      `cost` int(11) NOT NULL,
                                                      `filename` varchar(50) NOT NULL,
                                                      `descr` varchar(500) NOT NULL,
                                                      `value` int(11) NOT NULL DEFAULT '0',
                                                      PRIMARY KEY (`itemId`)
                                                      ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;";

  const CREATE_TABLE_LANGUAGE = "CREATE TABLE IF NOT EXISTS `language` (
                                                      `languageId` int(11) NOT NULL AUTO_INCREMENT,
                                                      `name` varchar(45) NOT NULL,
                                                      PRIMARY KEY (`languageId`)
                                                      ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;";

  const CREATE_TABLE_LOGIN = "CREATE TABLE IF NOT EXISTS `login` (
                                                        `userId` int(11) NOT NULL AUTO_INCREMENT,
                                                        `name` varchar(50) NOT NULL,
                                                        `password` varchar(256) NOT NULL,
                                                        `email` varchar(100) NOT NULL,
                                                        `token` char(64) NOT NULL,
                                                        `emailActive` char(64) DEFAULT NULL,
                                                        `active` int(11) NOT NULL DEFAULT '0',
                                                        `role` int(11) NOT NULL DEFAULT '0',
                                                        `codarId` int(11) NOT NULL,
                                                        PRIMARY KEY (`userId`)
                                                        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;";

  const CREATE_TABLE_MESSAGE = "CREATE TABLE IF NOT EXISTS `message` (
                                                          `messageId` int(11) NOT NULL AUTO_INCREMENT,
                                                          `codarId` int(11) NOT NULL,
                                                          `message` varchar(500) NOT NULL,
                                                          PRIMARY KEY (`messageId`)
                                                          ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;";

  const CREATE_TABLE_TRAINING = "CREATE TABLE IF NOT EXISTS `training` (
                                                          `trainingId` int(11) NOT NULL AUTO_INCREMENT,
                                                          `name` varchar(30) NOT NULL,
                                                          `value` int(11) NOT NULL,
                                                          `timelock` int(11) NOT NULL,
                                                          PRIMARY KEY (`trainingId`)
                                                        ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;";

  const INSERT_INTO_LANGUAGE = "INSERT INTO `language` (`languageId`, `name`) VALUES (1, 'PHP'), (2, 'C#');";
  const INSERT_INTO_TRAINING = "INSERT INTO `training` (`trainingId`, `name`, `value`, `timelock`) VALUES (1, 'Föreläsning', 100, 10), (2, 'Hackaton', 300, 25), (3, 'Tutorial', 10, 2), (4, 'Diskussion', 20, 4);";
  
  public static function createDatabaseSQL($name) {
    return "CREATE DATABASE IF NOT EXISTS `$name` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;";
  } 
}