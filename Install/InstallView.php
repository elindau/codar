<?php

namespace view;

class InstallView extends MasterView {

	private $m_localSubKey = "localSubKey";
	private $m_webbSubKey = "webbSubKey";
	private $m_regAdminKey = "regadmin";
	private $m_regPassKey = "regpass";
	private $m_regPassRepeatKey = "regpassrep";
	private $m_regEmailKey = "regemail";
	private $m_regSubKey = "regsubmit";
	private $m_installLocalKey = "installKey";
	private $m_uninstallLocalKey = "uninstallKey";
	private $m_installWebbKey = "installWebKey";
	private $m_uninstallWebbKey = "uninstallWebKey";
	
	public function doLocalWebb() {
		return "
		<h2>1. Install - Server</h2>
		<p>Välj vilken server du är asnluten mot</p>
			<form action='' method='post'>
				<input type='submit' name='$this->m_localSubKey' value='Lokalt' />
				<input type='submit' name='$this->m_webbSubKey' value='Binero / webb' />
			</form>";
	}

	public function isLocalSet() {
		return isset($_POST[$this->m_localSubKey]) == true ? true : false;
	}

	public function isWebbSet() {
		return isset($_POST[$this->m_webbSubKey]) == true ? true : false;
	}

	public function doInstallUninstallLocal() {
		return "
		<h2>2. Install - Val</h2>
		<p>Välj avinstallation eller installation/repair</p>
			<form action='' method='post'>
				<input type='submit' name='$this->m_installLocalKey' value='Installera' />
				<input type='submit' name='$this->m_uninstallLocalKey' value='Avinstallera' />
			</form>";	
	}

	public function isInstallLocalSet() {
		return isset($_POST[$this->m_installLocalKey]) == true ? true : false;
	}

	public function isUninstallLocalSet() {
		return isset($_POST[$this->m_uninstallLocalKey]) == true ? true : false;
	}

	public function doInstallUninstallWebb() {
		return "
		<h2>2. Install - Val</h2>
		<p>Välj avinstallation eller installation/repair</p>
			<form action='' method='post'>
				<input type='submit' name='$this->m_installWebbKey' value='Installera' />
				<input type='submit' name='$this->m_uninstallWebbKey' value='Avinstallera' />
			</form>";	
	}

	public function isInstallWebbSet() {
		return isset($_POST[$this->m_installWebbKey]) == true ? true : false;
	}

	public function isUninstallWebbSet() {
		return isset($_POST[$this->m_uninstallWebbKey]) == true ? true : false;
	}

	public function doAdminForm() {
		return "
			<h2>3. Install - Admin</h2>
			<p>Välj ditt admin-login</p>
			<form action='' method='post'>
				<input type='text' id='name' name='$this->m_regAdminKey' placeholder='Användarnamn' required autofocus />
				<input type='password' id='pw' name='$this->m_regPassKey' placeholder='Lösenord' required />
				<input type='password' id='pwRepeat' name='$this->m_regPassRepeatKey' placeholder='Repetera lösenord' required />
				<input type='email' id='email' name='$this->m_regEmailKey' placeholder='Email' required />
				<input type='submit' name='$this->m_regSubKey' value='Registrera' />
			</form>";
	}



	public function getUsername() {
		if(isset($_POST[$this->m_regAdminKey])) {
			return $_POST[$this->m_regAdminKey];
		}
		return null;
	}

	public function getPassword() {
		if(isset($_POST[$this->m_regPassKey])) {
			return $_POST[$this->m_regPassKey];
		}
		return null;
	}

	public function getPasswordRepeat() {
		if(isset($_POST[$this->m_regPassRepeatKey])) {
			return $_POST[$this->m_regPassRepeatKey];
		}
		return null;
	}

	public function getEmail() {
		if(isset($_POST[$this->m_regEmailKey])) {
			return $_POST[$this->m_regEmailKey];
		}
		return null;
	}

	public function triedToRegister() {
		return isset($_POST[$this->m_regSubKey]);
	}
}