<?php
namespace controller;

class RegisterUserC {

	private $m_view = null;

	public function __construct(\view\RegisterUser $view) {
		$this->m_view = $view;
	}

	public function doControll(\DBAccess\Database $db) {
		$registerView = $this->m_view;
		$registerHandler = new \model\RegisterUserH($db);

		// Kan bara registrera ny användare om man är utloggad
		if(!(\model\LoginHandler::isLoggedIn())) {
			$html = "";
			$message = "";

			if($registerView->triedToRegister()) {
				$user = $registerHandler->registerUser($registerView->getUsername(), 
										$registerView->getPassword(), 
										$registerView->getPasswordRepeat(), 
										$registerView->getEmail());
				
				// Finns fel att skriva ut!
				if(gettype($user) == \Common\Strings::t_array) {
					foreach($user as $err) {
						$registerView->setError($err);
					}
				} else {
					$registerView->setSuccess(\Common\Strings::s_register);
				}
			}
			$html = $registerView->doRegisterForm();
			$html .= $registerView->doBackButton();

			return $html;
		}
	}
}