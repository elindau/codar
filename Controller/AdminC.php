<?php

namespace controller;

class AdminC {
	
	private $m_view = null;
	private $m_oldPw = null;

	public function __construct(\view\AdminView $view) {
		$this->m_view = $view;
	}

	public function doControll(\DBAccess\Database $db) {

		$adminHandler = new \model\AdminH($db);
		$adminView = $this->m_view;
		$message = "";
		$html = "";

		// Har delete tryckts?
		if($adminView->didDelete()) {
			// Ta bort användaren
			if($adminHandler->deleteUser($adminView->getUserId())) {
				// Success
				$adminView->setSuccess(\Common\Strings::s_delete);
			} else {
				$adminView->setError(\Common\Strings::e_delete);
			}
		}

		// Har spara tryckts?
		if($adminView->didSaveEdit()) {
			// Uppdatera användaren
			$password = null;
			$passwordRepeat = null;

			// Hantera krypteringen 
			if($adminView->getOldPassword() == $adminView->getPassword()) {

				// Redan krypterat
				$password = $adminView->getPassword();
				$passwordRepeat = $adminView->getPasswordRepeat();
			} else {

				// Kryptera
				$encrypt = new \Common\Encrypt();
				$password = $encrypt->encryptPassword($adminView->getPassword());
				$passwordRepeat = $encrypt->encryptPassword($adminView->getPasswordRepeat());
			}

			$outcome = $adminHandler->updateUser($adminView->getUserId(), $adminView->getUsername(), $password, $passwordRepeat, $adminView->getEmail(), $adminView->getActive());

			if(is_array($outcome)) {
				foreach($outcome as $error) {
					$adminView->setError($error);
				}
			} else {
				$adminView->setSuccess("Lyckad uppdatering av användare!");
			}
		}

		// Har lägg till användare tryckts?
		if($adminView->didDoAdd()) {
			$outcome = $adminHandler->addUser($adminView->getUsername(), $adminView->getPassword(), $adminView->getPasswordRepeat(), $adminView->getEmail());
			if(is_array($outcome)) {
				foreach($outcome as $error) {
					$adminView->setError($error);
				}
			} else {
				$adminView->setSuccess(\Common\Strings::s_register);
			}
		}

		// Har edit tryckts
		if($adminView->didEdit()) {
			// Hämta ut användare
			$user = $adminHandler->getUser($adminView->getUserId());

			// Presentera Edit-formulär
			$html = $adminView->doEditForm($user);
		} else if($adminView->didAdd()) {
			$html = $adminView->doAddForm();
		} else {
			// HTML att visa
			$users = $adminHandler->getUsers();
			$html .= $adminView->doDeleteUserForm($users);
		}

		return $html;
	}

}