<?php

namespace controller;

class BaseController {

	public static function setViewMessages($view) {

		$html = "";

		// Finns det några felmeddelanden
		if(!($view->isValid())) {
			$html .= $view->doErrors();
		} else if($view->isMessage()){
			$html .= $view->doMessages();
		}

		return $html;
	}
}