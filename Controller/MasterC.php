<?php
namespace controller;

// Styr de andra controllerna och generarar output
class MasterController extends BaseController {

	public static function doControll() {
        // Kör config-filen
        $cfg = new \config\Config();
        $cfg->run();

        $nav = new \view\Navigation();
        $db = new \DBAccess\Database();

        // Fånga databasfel
        try {
            $db->connect(new \DBAccess\Settings());

            $site = "";

            // Vilken kontroller
            $site = self::switchControllers($nav->getActiveType(), $db);
        
            $db->close();

        } catch(\Exception $e) {
            // Logga fel och visa generisk error-page
            $err = $e->GetMessage();
            trigger_error($err);
        }
        
        return $site;
    }

    // Vilken kontroller som ska köras
    private static function switchControllers($activeController, \DBAccess\Database $db) {
        $views = array();
        $message = "";

        // Lägg till vyerna så meddelanden kan kontrolleras
        $standardView = new \view\StandardView();
        $views[] = $standardView;

        $loginView = new \view\LoginView();
        $views[] = $loginView;
        $loginController = new \controller\LoginController($loginView);

        $registerView = new \view\RegisterUser();
        $views[] = $registerView;
        $registerController = new \controller\RegisterUserC($registerView);
        
        $validMailView = new \view\ValidMailView();
        $views[] = $validMailView;
        $validMailController = new \controller\ValidMailController($validMailView);

        $forgotPwView = new \view\ForgotPasswordView();
        $views[] = $forgotPwView;
        $forgotPwController = new \controller\ForgotPasswordController($forgotPwView);

        $page = new \Common\PageView();
        $body = "";

        // Vilken controller vid utlogg ska köras?
        switch($activeController) {
            case \view\Navigation::UserViewsRegisterForm:
                $body = $registerController->doControll($db);
            break;

            // Samma som default just nu
            case \view\Navigation::UserViewsMain:
                $body = $standardView->doMainPage();
            break;

            // Aktivera mail
            case \view\Navigation::UserValidatesEmail:
                $body = $validMailController->doControll($db);
            break;

            // Glömt lösenord
            case \view\Navigation::UserForgotPw:
                $body = $forgotPwController->doControll($db);
            break;

            default:
                $body = $standardView->doMainPage();
            break;
        }

        // Login visas alltid
        $loginBar = $loginController->doControll($db);
        $infoBar = "";

        // Är användaren inloggad? - Loginförfarande och controllers
        $loginHandler = new \model\LoginHandler($db);
        if($loginHandler->isLoggedIn()) {

            // Nuvarande meny-alternativ
            $current = "";

            $mainView = new \view\MainView();
            $views[] = $mainView;
            $mainController = new \controller\MainController($mainView);

            // Hämta ut den inloggade användaren
            $user = $loginHandler->getLoggedInUser();

            // Var det första gången användaren loggade in (generera codar)
            if($user->getCodarId() == \model\User::FIRST_LOGIN) {
                $codarView = new \view\CodarView();
                $views[] = $codarView;
                $codarController = new \controller\CodarController($codarView);
                $body = $codarController->doControll($db);
            } else {

                // Uppdatera låset
                $lock = new \model\Lock($db);
                $lock->isLocked($user->getCodarId());

                // Visa alltid meddelande
                // TODO: Lägg till meddelande controller
                $messageView = new \view\MessageView();
                $views[] = $messageView;
                $messageController = new \controller\MessageController($messageView);
                $infoBar .= $messageController->doControll($db);
                
                // Annars switcha på inloggade controllers
                switch($activeController) {
                    case \view\Navigation::UserViewsCodar:
                        $body = $mainController->doControll($db, $user);

                        $current = \view\Navigation::UserViewsCodar;
                    break;

                    case \view\Navigation::UserViewsAttacks:
                        $attackView = new \view\AttackView();
                        $views[] = $attackView;
                        $attackController = new \controller\AttackController($attackView);
                        $body = $attackController->doControll($db);

                        $current = \view\Navigation::UserViewsAttacks;
                    break;

                    case \view\Navigation::UserViewsTraining:
                        $trainingView = new \view\TrainingView();
                        $views[] = $trainingView;
                        $trainingController = new \controller\TrainingController($trainingView);
                        $body = $trainingController->doControll($db);

                        $current = \view\Navigation::UserViewsTraining;
                    break;

                    case \view\Navigation::UserViewsShop: 
                        $shopView = new \view\ShopView();
                        $views[] = $shopView;
                        $shopController = new \controller\ShopController($shopView);
                        $body = $shopController->doControll($db);

                        $current = \view\Navigation::UserViewsShop;
                    break;

                    case \view\Navigation::AdminView:
                        if($user->getRole() == \model\User::ADMIN) {
                            $adminView = new \view\AdminView();
                            $views[] = $adminView;
                            $adminController = new \controller\AdminC($adminView);
                            $body = $adminController->doControll($db);

                            $current = \view\Navigation::AdminView;
                        }
                    break;

                    default:
                        // Annars generera utgångspunkt (main/startingpoint)
                        $body = $mainController->doControll($db, $user);

                        $current = \view\Navigation::UserViewsCodar;
                    break;
                }
            }

            // Skapa annorlunda meny
            $navHtml = $page->getLoginNavigation($user->getCodarId(), $current);

            if($user->getRole() == \model\User::ADMIN) {
                // Lägg till admin-panelen
                $navHtml = $page->getLoginNavigation($user->getCodarId(), $current, true);
            }

        } else {
            // Meny vid utlogg
            $navHtml = $page->getNavigation();
        }

        $title = \Common\Strings::m_title;

        // Finns det några meddelanden på någon vy?
        foreach($views as $view) {
           $message .= parent::setViewMessages($view); 
        }
        
        // Vilken design som ska köras
        if($page->isStyle1Set()) {
            \Common\Apperence::setStyle(\Common\Apperence::STYLE1);
        } else if($page->isStyle2Set()) {
            \Common\Apperence::setStyle(\Common\Apperence::STYLE2);
        }

        $loginHtml = $page->getLogin($loginBar);
        $headerHtml = $page->getHeader($title);
        $bodyHtml = $page->getPage($body, $message);
        $infoHtml = $page->getInfoBar($infoBar);

        $site = $headerHtml . $loginHtml . $navHtml . $infoHtml . $bodyHtml;

        return $site;
    }
}