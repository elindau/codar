<?php

namespace controller;

class ValidMailController {

	private $m_view = null;

	public function __construct(\view\ValidMailView $view) {
		$this->m_view = $view;
	}

	public function doControll(\DBAccess\Database $db) {
		$this->m_db = $db;
		$vmview = $this->m_view;

		// Bör inte vara inloggad
		// isLoggedin
		if(!(\model\LoginHandler::isLoggedIn())) {
			$vmhandler = new \model\ValidMailHandler($db);
			$message = "";

			// Finns idt i get
			if($vmview->getUserId() != null) {
				// Finns id't i databasen och är användaren oaktiv..
				$user = $vmhandler->isNonActive($vmview->getUserId());
				if(!(is_numeric($user))) {
					// .. aktivera användaren
					$outcome = $vmhandler->activateUser($user);
					if($outcome) {
						// Success
						$vmview->setSuccess(\Common\Strings::s_activate);
					} else {
						$vmview->setError(\model\ValidMailHandler::ERROR_COULDNT_ACTIVATE);
					}
				} else {
					// Något gick fel
					$vmview->setError($user);
				}
			}
		}

		$finalHtml = "";

		$finalHtml .= $vmview->doReturn();

		return $finalHtml;
	}

}