<?php

namespace controller;

class CodarController {

	private $m_view = null;

	public function __construct(\view\CodarView $view) {
		$this->m_view = $view;
	}

	public function doControll(\DBAccess\Database $db) {

		$codarView = $this->m_view;
		$codarHandler = new \model\CodarHandler($db);
		$createdCodar = false;

		// Har användaren tryckt på acceptera
		if($codarView->didSubmit()) {
			// Plita in codaren i databasen och koppla codar+användare
			$outcome = $codarHandler->registerCodar($codarView->getCodarName(), $codarView->getLanguageId());
			if($outcome) {

				// Gör en header reloc
				header ("Location: http://{$_SERVER['HTTP_HOST']}". \view\Navigation::generateCodarGet() . $codarHandler->getInsertedCodarId() ); 
			} else if (is_numeric($outcome)){
				$codarView->setError($outcome);
			}
		}

		// Hämta ut språk som är tillgängliga för codarn
		$languages = $codarHandler->getAllLanguages();

		$finalHtml = "";
		// Generera formulär och populera, men inte efter success
		if(!($createdCodar)) {
			$finalHtml .= $codarView->doCodarForm($languages);
		}

		return $finalHtml;
	}
}