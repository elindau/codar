<?php

namespace controller;

class AttackController {

	private $m_view = null;

	public function __construct(\view\AttackView $view) {
		$this->m_view = $view;
	}

	public function doControll(\DBAccess\Database $db) {
		$attackView = $this->m_view;
		$attackHandler = new \model\AttackHandler($db);

		// Har användaren tryckt på attackera
		if($attackView->didAttack()) {
			// Hämta ut codar att attackera
			$codarId = $attackView->getCodarToAttack();

			if($codarId != null) {
				// Attackera
				$outcome = $attackHandler->attackCodar($codarId);

				// Outcome
				switch($outcome) {
					case \model\AttackHandler::WON:
						$attackView->setSuccess("Du vann och tjänade " . $attackHandler->getEarnedExp() . " XP och " . $attackHandler->getEarnedMoney() . " " . \model\Money::SIGN);
					break;

					case \model\AttackHandler::LOST:
						$attackView->setSuccess("Du förlorade " . $attackHandler->getLostMoney() . \model\Money::SIGN . "!");
					break;

					case \model\AttackHandler::ERROR_NO_CODAR:
						$attackView->setError("Ingen Codar kunde hittas");
					break;

					case \model\AttackHandler::ERROR_LOCKED:
						$attackView->setError(\Common\Strings::e_lockedCodar);
					break;

					case \model\AttackHandler::ERROR_NO_ENERGY:
						$attackView->setError("Din Codar har ingen energi kvar!");
					break;
				}
			} else {
				// Fel
				$attackView->setError("Ingen Codar kunde hittas");
			}
		}

		// Populera en lista med andra codars
		$codars = $attackHandler->getAllCodarsExceptUsers();
		$html = $attackView->doAllCodarsList($codars);

		return $html;

	}
}