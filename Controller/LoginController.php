<?php
namespace controller;

class LoginController {

	private $m_user = null;
	private $m_view = null;

	public function __construct(\view\LoginView $view) {
		$this->m_view = $view;
	}

	public function doControll(\DBAccess\Database $db) {
		$loginView = $this->m_view;
		try {
			$loginHandler = new \model\LoginHandler($db);
		} catch (\Exception $e){
			$loginView->setError($e->getMessage());
		}

		// Om användaren är inloggad
		if($loginHandler->isLoggedIn()) {

			// Om logout är tryckt, logga ut om det går
			if($loginView->triedToLogout()) {
				$loginHandler->doLogout();

				// Förstör sparad kaka vid logout
				$loginView->dropCookie();
				// $loginView->setSuccess(\Common\Strings::m_logout);
			}

		} else {

			// Finns det sparade kakor med användaruppgifter
			if($loginView->getTokenFromCookie()) {
				// Logga in med token
				$loginHandler->doCookieLogin($loginView->getTokenFromCookie());
			} else {
				// Om submit är tryckt, logga in om det går
				if($loginView->triedToLogin()) {
					// Om det är rätt uppgifter
					if($loginHandler->doLogin($loginView->getUsername(), $loginView->getPassword())) {
						//$loginView->setSuccess(\Common\Strings::m_login);
							// Var remember me ikryssat
							if($loginView->checkedRememberMe()) {

								// Spara undan användarens token i Cookie
								$loginView->doRememberMe($loginHandler->getToken());
							}

					} else {
						$loginView->setError(\Common\Strings::e_wrongUserOrPw);
					}
				} 
			}
		}

		// Vad som ska visas, samt vilket meddelande
		$finalHtml = "";
		if($loginHandler->isLoggedIn()) {
			$user = $loginHandler->getLoggedInUser();
			$finalHtml = $loginView->doLogoutBox($user->getUsername());
		} else {
			$finalHtml = $loginView->doLoginBox();
		}

		return $finalHtml;
	}
}