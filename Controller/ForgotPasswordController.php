<?php

namespace controller;

class ForgotPasswordController {

	private $m_view = null;

	public function __construct(\view\ForgotPasswordView $view) {
		$this->m_view = $view;
	}

	public function doControll(\DBAccess\Database $db) {
		
		$forgotPwView = $this->m_view;
		$forgotPwHandler = new \model\ForgotPasswordHandler($db);
		$message = "";
		$finalHtml = "";
		// Har submit tryckts
		if($forgotPwView->triedToSend()) {
			// Försök skicka mail till given adress
			$outcome = $forgotPwHandler->sendEmail($forgotPwView->getEmail());
			if(!is_numeric($outcome)) {
				$forgotPwView->setSuccess(\Common\Strings::s_pwreset);
			} else {
				// Annars fans det fel
				$forgotPwView->setError($outcome);
			}
		}

		/*$finalHtml = "";
		// Finns det några felmeddelanden
		if(!($forgotPwView->isValid())) {
			$finalHtml .= $forgotPwView->doErrors();
		} else if($forgotPw->isMessage()){
			$finalHtml .= $forgotPwView->doSuccess($message);
		} */

		$finalHtml .= $forgotPwView->doForgotPwMail();
		$finalHtml .= $forgotPwView->doBackButton();
		return $finalHtml;

	}
}