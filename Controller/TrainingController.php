<?php

namespace controller;

class TrainingController {

	private $m_view = null;

	public function __construct(\view\TrainingView $view) {
		$this->m_view = $view;
	}

	public function doControll(\DBAccess\Database $db) {
		$trainingView = $this->m_view;
		$trainingHandler = new \model\TrainingHandler($db);

		// Har skicka på träning tryckts
		if($trainingView->isTrainingPressed()) {
			$trainingId = $trainingView->getTrainingType();
			// Skicka Codar på träning
			$outcome = $trainingHandler->sendCodarTraining($trainingId);

			if(is_numeric($outcome)) {
				$trainingView->setError($outcome);
			} else {
				$trainingView->setSuccess("Du skickade din Codar på träning!");
			}
		}

		// Har köp träning tryckts
		if($trainingView->isBuyPressed()) {
			$trainingId = $trainingView->getBuyTrainingType();
			$outcome = $trainingHandler->buyTraining($trainingId);

			if(is_numeric($outcome)) {
				$trainingView->setError($outcome);
			} else {
				$trainingView->setSuccess("Lyckat köp!");
			}
		}

		// Generera köp-dropdown
		$buyTrainingTypes = $trainingHandler->getAllTrainingTypes();
		$buyHtml = $trainingView->doBuyForm($buyTrainingTypes);

		// Generera en dropdown
		$trainingTypes = $trainingHandler->getAllAvailableTrainingTypes();
		$trainingHtml = $trainingView->doTrainingForm($trainingTypes);

		// TODO: Merge
		$html = $trainingHtml.$buyHtml;
		return $html;
	}
}