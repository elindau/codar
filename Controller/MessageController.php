<?php

namespace controller;

class MessageController {

	private $m_view = null;

	public function __construct(\view\MessageView $view) {
		$this->m_view = $view;
	}

	public function doControll(\DBAccess\Database $db) {
		$messageView = $this->m_view;
		$messageHandler = new \model\MessageHandler($db);

		// Har deleteAll tryckts
		if($messageView->didDeleteAll()) {
			$messageHandler->deleteAllMessages();
		}

		// Har delete tryckts
		if($messageView->didDelete()) {
			// Hämta ut meddelandeIdt och ta bort
			$messageHandler->deleteMessage($messageView->getMessageId());
		}

		// Hämta ut alla meddelanden för din codar
		$messages = $messageHandler->getAllMessages();

		$button = "";
		if(count($messages) == 0) {
			$button = $messageView->doNoMessageButton();
		} else {
			$button = $messageView->doGotMessageButton($messages);
		}

		return $button;
	}
}