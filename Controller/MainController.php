<?php

namespace controller;

// Hanterar startsidan vid inlogg
class MainController {

	private $m_view = null;

	public function __construct(\view\MainView $view) {
		$this->m_view = $view;
	}

	public function doControll(\DBAccess\Database $db, \model\User $user = null) {

		$mainView = $this->m_view;
		$mainHandler = new \model\MainHandler($db);
		$codarHtml = "";
		// Hämta ut Codar
		$codarId = null;

		if($mainView->getCodarId() != null) {
			// CodarIdt finns i GET
			$codarId = $mainView->getCodarId();
		}

		if($user != null && $mainView->getCodarId() == null) {
			// CodarIdt finns i User-objektet
			$codarId = $user->getCodarId();
		}

		if($user == null && $mainView->getCodarId() == null) {
			// Något har gått fel
			$mainView->setError(\Common\Strings::e_noCodar);
		}

		// Hämta ut top-5 codars
		$codars = $mainHandler->getTopCodars();

		// Visa din Codars nuvarande status
		$codar = $mainHandler->getCodar($codarId);
		if(is_numeric($codar)) {
			$mainView->setError($codar);
		} else {
			$lock = new \model\Lock($db);
			$codarHtml = $mainView->presentCodar($codar, $lock->getCodarTimeLock($codarId));
		}

		// Visa topplista
		$topHtml = $mainView->doToplist($codars);


		return $codarHtml.$topHtml;
	} 
}