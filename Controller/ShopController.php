<?php

namespace controller;

class ShopController {

	private $m_view = null;

	public function __construct(\view\ShopView $view) {
		$this->m_view = $view;
	}

	public function doControll(\DBAccess\Database $db) {
		$shopView = $this->m_view;
		$shopHandler = new \model\ShopHandler($db);

		// Tryckte på köp
		if($shopView->didBuy()) {
			$outcome = $shopHandler->buyItem($shopView->getItemToBuy());

			if(is_numeric($outcome)) {
				$shopView->setError($outcome);
			} else {
				$shopView->setSuccess("Lyckat köp!");
			}
		}

		// Tryckte admin på lägg till
		if($shopView->didAdd()) {
			$outcome = $shopHandler->addItem($shopView->getFile(), $shopView->getItemName(), $shopView->getItemCost(), $shopView->getItemValue());
			if(is_array($outcome)) {
				foreach($outcome as $error) {
					$shopView->setError($error);
				}
			}
		}

		// Tryckte admin på delete
		if($shopView->didDelete()) {
			$outcome = $shopHandler->deleteItem($shopView->getItemToBuy());

			if($outcome) {
				$shopView->setSuccess("Lyckad borttagning!");
			} else {
				$shopView->setError($outcome);
			}
		}


		// Populera en lista med alla items
		$items = $shopHandler->getAllItems();

		// Är användaren admin?
		$admin = false;
		$user = \model\LoginHandler::getLoggedInUser();

		if($user->getRole() == \model\User::ADMIN) {
			$admin = true;
		}

		$html = "";
		if($shopView->didAdminAdd()) {
			$html = $shopView->doAddItemForm();
		} else {
			$html = $shopView->doShopList($items, $admin);
		}
		

		return $html;
	}
}